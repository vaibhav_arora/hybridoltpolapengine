package messagingLayer;

import messaging.HybridEngine.AnalyticalOperationRequest;
import messaging.HybridEngine.AnalyticalOperationResponse;
import messaging.HybridEngine.ApplicationClientRequest;
import messaging.HybridEngine.ApplicationClientResponse;
import messaging.HybridEngine.HybridStoreService;
import messaging.HybridEngine.TransactionalBatchRequest;
import messaging.HybridEngine.TransactionalBatchResponse;
import messaging.HybridEngine.TransactionalBeginRequest;
import messaging.HybridEngine.TransactionalCommitRequest;
import messaging.HybridEngine.TransactionalOperationRequest;
import messaging.HybridEngine.TransactionalOperationResponse;

import org.apache.log4j.Logger;

import transactionalClient.TransactionalClient;
import transactionalEngine.TransactionalManager;
import utilities.Operation;
import utilities.OperationResult;
import utilities.TransactionBatch;
import analyticalEngine.AnalyticalEngine;

import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;

public class MessagingCoordinator {
  
  private TransactionalClient transactionalClient;
  private AnalyticalEngine analyticalEngine;
  private TransactionalManager transactionalManager;
  private final static Logger LOG = Logger.getLogger(MessagingCoordinator.class);
  
  public MessagingCoordinator(TransactionalClient transactionalClient, 
		  AnalyticalEngine analyticalEngine, 
		  TransactionalManager transactionalManager){
	  
	  this.transactionalClient = transactionalClient;
	  this.analyticalEngine = analyticalEngine;
	  this.transactionalManager = transactionalManager;
  }
  
  public void sendTransactionalBatchUpdateToAnalyticalEngine(){
	  
  }
  
  public HybridStoreService initHybridStoreService() {
      return (new RemoteServiceHandler());
  }
  
  //Hybrid Service Methods
   private class RemoteServiceHandler extends HybridStoreService {

	@Override
	public void beginTransaction(RpcController controller,
			TransactionalBeginRequest request,
			RpcCallback<TransactionalOperationResponse> done) {
		
		boolean begin_status = transactionalManager.beginTransaction(request.getTransactionId());
		
		TransactionalOperationResponse transactionalOperationResponse = TransactionalOperationResponse.newBuilder().
				setTransactionId(request.getTransactionId()).setOperationSuccessful(begin_status).build();
		done.run(transactionalOperationResponse);
	}

	@Override
	public void transactionalOperation(RpcController controller,
			TransactionalOperationRequest request,
			RpcCallback<TransactionalOperationResponse> done) {
		// TODO Auto-generated method stub
		String transactionOperationQuery = request.getOperation();
		Operation transactionalOperation = new Operation(transactionOperationQuery);
		OperationResult transactionOperationResult = null;
		TransactionalOperationResponse.Builder transactionalOperationResponseBuilder = 
				TransactionalOperationResponse.newBuilder();
		
		//Parse the operation to see if it is a read or a write operation
		if(transactionalOperation.isAReadOperation()){
			transactionOperationResult = transactionalManager.processReadRequest(request.getTransactionId(), 
					transactionalOperation);
			transactionalOperationResponseBuilder.setOperationSuccessful(transactionOperationResult.
					getOperationStatus())
			.setTransactionId(request.getTransactionId());
			//TODO : Process the tuples
		} else {
			transactionOperationResult = transactionalManager.processWriteRequest(request.getTransactionId(), 
					transactionalOperation);
			transactionalOperationResponseBuilder.setOperationSuccessful(transactionOperationResult.
					getOperationStatus())
			.setTransactionId(request.getTransactionId());
			//TODO : Process the tuples 
		}
		
		done.run(transactionalOperationResponseBuilder.build());
	}

	@Override
	public void commitTransaction(RpcController controller,
			TransactionalCommitRequest request,
			RpcCallback<TransactionalOperationResponse> done) {
		
		boolean commit_status = transactionalManager.processCommitRequest(request.getTransactionId());
		
		TransactionalOperationResponse transactionalOperationResponse = TransactionalOperationResponse.newBuilder().
				setTransactionId(request.getTransactionId()).setOperationSuccessful(commit_status).build();
		done.run(transactionalOperationResponse);
		
	}

	@Override
	public void analyticalOperation(RpcController controller,
			AnalyticalOperationRequest request,
			RpcCallback<AnalyticalOperationResponse> done) {
		
		Operation analyticalOperation = new Operation(request.getQuery());
		OperationResult analytical_operation_result = analyticalEngine.executeQuery(analyticalOperation);
		
	}

	@Override
	public void transferTransactionalBatch(RpcController controller,
			TransactionalBatchRequest request,
			RpcCallback<TransactionalBatchResponse> done) {
		
		TransactionBatch transactionBatch = new TransactionBatch(request.getBatchId(), 
				request.getTransactionListList(), request.getWaitToIngest());
		analyticalEngine.receiveBatchFromAnalyticalEngine(request.getPartionId(), transactionBatch);	
	}

	@Override
	public void processDataOperation(RpcController controller,
			ApplicationClientRequest request,
			RpcCallback<ApplicationClientResponse> done) {
		// Invoke transactional client to handle the operation
		ApplicationClientResponse.Builder applicationClientResponseBuilder = 
				ApplicationClientResponse.newBuilder();
		try {
			OperationResult operationResult = 
					transactionalClient.routeClientOperation(request.getDataOperation());
			applicationClientResponseBuilder.setOperationSuccessful(operationResult.getOperationStatus());
		} catch (Exception e) {
			LOG.error("Exception occured while routing request",e);
			applicationClientResponseBuilder.setOperationSuccessful(false);
		} 
		done.run(applicationClientResponseBuilder.build());
	}
	   
   }
  

}
