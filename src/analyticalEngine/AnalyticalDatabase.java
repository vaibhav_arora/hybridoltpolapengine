package analyticalEngine;

import messaging.HybridEngine.SerializedTransaction;
import utilities.Operation;
import utilities.OperationResult;

public interface AnalyticalDatabase {
  
  public OperationResult executeQuery(Operation databaseQuery);
  
  public Boolean batchCommit(SerializedTransaction transaction);

}
