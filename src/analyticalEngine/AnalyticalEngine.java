package analyticalEngine;

import org.apache.log4j.Logger;

import utilities.Operation;
import utilities.OperationResult;
import utilities.TransactionBatch;

public class AnalyticalEngine {
  
  AnalyticalDatabase analyticalDatabase;
  ColumnStoreDeltaManager columnStoreDelta;
  public final static Logger LOG = Logger.getLogger(AnalyticalDatabase.class);
  
  public AnalyticalEngine(ColumnStoreDeltaManager columnStoreDelta, AnalyticalDatabase analyticalDatabase) {
	  this.columnStoreDelta = columnStoreDelta;
	  this.analyticalDatabase = analyticalDatabase;
  }
  
  public void receiveBatchFromAnalyticalEngine(int rowPartitionId, TransactionBatch transactionBatch) {
	  
	  if(transactionBatch.getTransactionList()==null) {
		  LOG.error("Transactional Batch received is empty");
	  }
	  
	  LOG.info("Received batch of changes from the Transactional " +
	  		"Engine with Id:" + transactionBatch.getBatchId() + " and batch size:" + 
			  transactionBatch.getTransactionList().size() + " from row partition with Id:" + rowPartitionId) ;
	  
	  //Ingest the changes to the Delta
	  columnStoreDelta.ingestTransactionBatchToDelta(rowPartitionId, transactionBatch);
	  
  }
  
  public OperationResult executeQuery(Operation analyticalQuery) {
	  OperationResult operationResult = null;
	  
	  operationResult = analyticalDatabase.executeQuery(analyticalQuery);
	  
	  return operationResult;
  }

}
