package analyticalEngine;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import messaging.HybridEngine.SerializedTransaction;

import org.apache.log4j.Logger;

import utilities.Constants;
import utilities.TransactionBatch;

/**
 * Maintains the delta changes which are sent from the row store.
 * Runs periodically and ingests the delta into the columnar analytical database
 * Also takes care of changes ingesting into the delta store from the row stores
 * @author vaibhav
 *
 */
public class ColumnStoreDeltaManager implements Runnable {
	
	// Mapping from each batchID to a mapping of partition to the respective batch to 
	// ingest from that partition
	Map<Integer, Map<Integer, TransactionBatch>> transactionBatchesToIngest;
	int batchIDToIngest;
	AnalyticalDatabase analyticalDatabase;
	// Currently populated through constructor. Later on we can make an API call
	int rowPartitionSize;
	ConcurrentHashMap<Integer, AtomicInteger> currentNumOfPartitionsPerBatch;
	
	public final static Logger LOG = Logger.getLogger(ColumnStoreDeltaManager.class);
	
	public ColumnStoreDeltaManager(int batchIDToIngest, int rowPartitionSize, 
	    AnalyticalDatabase analyticalDatabase) {
	  this.transactionBatchesToIngest = new HashMap<Integer, Map<Integer, TransactionBatch>>();	
	  this.batchIDToIngest = batchIDToIngest;
	  this.rowPartitionSize = rowPartitionSize;
	  this.currentNumOfPartitionsPerBatch = new ConcurrentHashMap<Integer, AtomicInteger>();
	  this.analyticalDatabase = analyticalDatabase;
	}
	
	public void ingestTransactionBatchToDelta(int partitionID, 
	    TransactionBatch transactionBatch){
		
		Boolean waitToIngest = transactionBatch.getHasDistributedtransactions();
		Integer batchID = transactionBatch.getBatchId();
		
		if(!waitToIngest){
			// Ingest changes to column store right away since we don't have to wait
			Boolean isBatchCommitted = ingestChangesToColumnStore(partitionID, transactionBatch);
			//TODO : Retry Logic if isBatchCommit replies false;
			
			if(!this.currentNumOfPartitionsPerBatch.containsKey(batchID)){
				AtomicInteger atomicInteger = new AtomicInteger();
				this.currentNumOfPartitionsPerBatch.putIfAbsent(batchID, atomicInteger);
			}
			// Increment the number of partitions received for the given batchId 
			this.currentNumOfPartitionsPerBatch.get(batchID).incrementAndGet();
			return;
		}
		
		// If there are distributed transactions add the batch to the list of batches to be ingested
		// with the same batchId
		if(!transactionBatchesToIngest.containsKey(batchID)){
			transactionBatchesToIngest.put(batchID, new HashMap<Integer, TransactionBatch>());
		}
		transactionBatchesToIngest.get(batchID).put(partitionID, transactionBatch);
	}
	
	public Boolean ingestChangesToColumnStore(int partitionID, TransactionBatch transactionBatch) {
		LOG.info("Ingesting batchId:"+transactionBatch.getBatchId()+" from partitionId:"+partitionID +" into the " +
				"column store");
		Boolean isBatchCommitted = false;
		
		// We just set the transaction Id for the batched transaction to -1
		SerializedTransaction.Builder batchedTransactionBuilder = SerializedTransaction.newBuilder().setTransactionId(-1);
		int count = 0;
		long avgCommitTime = 0;
		for(SerializedTransaction serializedTransaction: transactionBatch.getTransactionList()){
			// Compile all the transactions into a single transaction and batch commit
			avgCommitTime += serializedTransaction.getOperationCommitTime();
			count++;
			for(String updateOperation: serializedTransaction.getUpdateOperationList()){
				batchedTransactionBuilder.addUpdateOperation(updateOperation);
			}	
		}
		avgCommitTime /= count;
		batchedTransactionBuilder.setOperationCommitTime(avgCommitTime);
		// Batch commit the serialized transactions from the batch to the column store 
		isBatchCommitted = this.analyticalDatabase.batchCommit(batchedTransactionBuilder.build());	
		return isBatchCommitted;
	}
	
	@Override
	public void run() {
		
		//Runs periodically 
		while(true) {
			
			int partitionBatchWaitingSize;
			if(transactionBatchesToIngest.containsKey(batchIDToIngest)) {
				Map<Integer, TransactionBatch> partitionToBatchMap = transactionBatchesToIngest.
						get(batchIDToIngest);
				partitionBatchWaitingSize = partitionToBatchMap.size();
			} else {
				partitionBatchWaitingSize = 0;
			}

			if(this.currentNumOfPartitionsPerBatch.get(batchIDToIngest).intValue() + 
					partitionBatchWaitingSize == rowPartitionSize){
				//Check if there are any transaction batches to ingest 
				// If there are then ingest all the partitions waiting to be ingested for that batchId into the column store
				
				// Increase the batchId to Ingest
				synchronized (this) {
					batchIDToIngest++;
				}
			}
			
			try {
				Thread.sleep(Constants.BATCH_INGESTING_POLL_PERIOD);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

}
