package utilities;

public class OperationResult {
	
	Boolean operationStatus;
	
	public OperationResult() {
		this.operationStatus = false;
	}
	
	public void setOperationStatus(Boolean operationStatus){
		this.operationStatus = operationStatus;
	}
	
	public Boolean getOperationStatus() {
		return this.operationStatus;
	}

}
