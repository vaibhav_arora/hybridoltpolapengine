package utilities;

import java.util.ArrayList;
import java.util.List;

import messaging.HybridEngine.SerializedTransaction;

public class TransactionBatch {

	List<SerializedTransaction> transactionList;
	Integer batchId;
	Boolean hasDistributedTransactions;
	
	public TransactionBatch(Integer batchId){
		this.batchId = batchId;
		this.transactionList = new ArrayList<SerializedTransaction>();
		//TODO: Currently it is just set has false, while transactions are added
		// we should check for distributed transactions and if there are then this flag
		//should be toggled
		this.hasDistributedTransactions = false;
	}
	
	/**
	 * Invoked from the analytical engine
	 * @param batchId
	 * @param transactionList
	 */
	public TransactionBatch(Integer batchId, List<SerializedTransaction> 
	  transactionList, Boolean hasDistributedTransactions) {
		this.batchId = batchId;
		this.transactionList = transactionList;
		this.hasDistributedTransactions = hasDistributedTransactions;
	}
	
	public List<SerializedTransaction> getTransactionList() {
		return this.transactionList;
	}
	
	public Integer getBatchId() {
		return this.batchId;
	}
	
	public Boolean getHasDistributedtransactions() {
		return this.hasDistributedTransactions;
	}

	public void addTransaction(Transaction transaction) {
		SerializedTransaction.Builder serializedTransactionBuilder = SerializedTransaction.newBuilder().setTransactionId(
				transaction.getTransactionId()).setOperationCommitTime(transaction.getOperationCommitTime());
		for(Operation writeOperations: transaction.getWriteOperations()){
			//TODO: A variation would be to create a memory copy of updated tuples rather than the copy
			// of the query like its done currently
			String writeOperation = new String(writeOperations.getQuery());
			serializedTransactionBuilder.addUpdateOperation(writeOperation);
		}
		this.transactionList.add(serializedTransactionBuilder.build());
	}
}
