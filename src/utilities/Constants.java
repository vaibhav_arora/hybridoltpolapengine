package utilities;

public class Constants {
  
  public static String READ = "Read";
  public static String WRITE = "Write";
  public static String ANALYTICAL = "Analytical";
  public static String QUERY_DELIMETER = " ";
  public static String BEGIN = "Begin";
  public static String COMMIT = "Commit";
  public static String TABLE_NAME = "TestTable";
  public static String VALUE_COLUMN = "value";
  public static String KEY_COLUMN = "table_key";
  public static String QUERY_FIELD_DELIMETER = "#";
  public static String SQL_FIELD_DELIMETER = ".";
  public static String SELECT = "SELECT";
  public static String INSERT = "INSERT";
  public static String UPDATE = "UPDATE";
  public static String WHERE = "WHERE";
  public static String AND = "AND";
  public static int REQUEST_INDEX = 2;
  public static int TRANSACTION_ID_INDEX = 1;
  public static int ANALYTICAL_ID_INDEX = 1;
  public static long BATCH_SENDING_PERIOD = 32L;
  public static long BATCH_INGESTING_POLL_PERIOD = 1000L;
  public static String RESULT_COLUMN_DELIMITER = "#";
  public static String RESULT_ROW_DELIMITER = "*";
}
