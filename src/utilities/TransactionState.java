package utilities;

public enum TransactionState {
	BEGIN,
	ACTIVE,
	COMMITTED,
	ABORTED;
}
