package utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Transaction {

  Long transactionId;
  Set<DataItem> readSet;
  Set<DataItem> writeSet;
  TransactionState transactionState;
  Set<Operation> operations;
  Set<Operation> writeOperations;
  long operationCommitTime;
  Boolean isADistributedTransaction = false;
  List<Integer> partitionsInvolved;
  Boolean isPrepared;
  
  public Transaction(Long transactionId){
    this.transactionId = transactionId;
    this.readSet = new HashSet<DataItem>();
    this.writeSet = new HashSet<DataItem>();
    this.transactionState = TransactionState.BEGIN;
    this.writeOperations = new HashSet<Operation>();
    this.operations = new HashSet<Operation>();
    this.partitionsInvolved = new ArrayList<Integer>();
    this.isPrepared = false;
  }
  
  public Long getTransactionId() {
	  return this.transactionId;
  }
  
  public TransactionState getState(){
	  return this.transactionState;
  }
  
  public void setState(TransactionState transactionState){
	  this.transactionState = transactionState;
  }
  
  public Set<DataItem> getReadSet(){
	  return this.readSet;
  }
  
  public Set<DataItem> getWriteSet(){
	  return this.writeSet;
  }
  
  public Set<Operation> getWriteOperations(){
	  return this.writeOperations;
  }
  
  public Set<Operation> getOperations(){
	  return this.operations;
  }
  
  public void addReadOperation(Operation readOperation){
	  this.getOperations().add(readOperation);
	  
	  for(DataItem readItem: readOperation.getReadSet()) {
		  this.readSet.add(readItem);
	  }
  }
  
  public void addWriteOperation(Operation writeOperation){
	  this.getOperations().add(writeOperation);
	  this.getWriteOperations().add(writeOperation);
	  for(DataItem tobeWrittenItem: writeOperation.getWriteSet()) {
		  this.writeSet.add(tobeWrittenItem);
	  }
  }
  
  public Long getOperationCommitTime() {
	  return this.operationCommitTime;
  }
  
  public void setOperationCommitTime(long operationCommitTime) {
	  this.operationCommitTime = operationCommitTime;
  }
  
  public void setTransactionDistributed(){
    this.isADistributedTransaction = true;
  }
  
  public Boolean isTransactionDistributed() {
    return this.isADistributedTransaction;
  }
  
  public List<Integer> getPartitionsInvolved(){
    return this.partitionsInvolved;
  }
  
  public void addPartition(Integer partitionId){
    this.partitionsInvolved.add(partitionId);
  }
  
  public void setTransactionToBePrepared() {
    this.isPrepared = true;
  }
}
