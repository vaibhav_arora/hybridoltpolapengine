package utilities;

import org.apache.log4j.Logger;

public class Utils {
	
	private final static Logger LOG = Logger.getLogger(Utils.class);

	/**
   * A quick and dirty method for testing with simple Read and Write statements 
   * @param query
   */
  public static String convertToSQLSelect(String query) {
    String[] queryParts = query.split(Constants.QUERY_DELIMETER);
    
    String[] fieldParts = queryParts[1].split(Constants.QUERY_FIELD_DELIMETER);
    // DB_NAME.TABLE_NAME
    //String tableName = fieldParts[0] + Constants.SQL_FIELD_DELIMETER + fieldParts[1];
    // Have the DbName in the connection url now.
    // This change is to remove the Db name from the sql as these statements are
    // being executed on column store as well
    String tableName = fieldParts[1];
    // Key is the attribute name. 
    String key = fieldParts[2];
    String keyValue = fieldParts[3];
    // Read the recored for a particular key Value
    String sqlStatement = "Select * from " + tableName + " where " + key + 
        " = " + keyValue ;
    
    //LOG.debug(sqlStatement);
    return sqlStatement;
  }
  
  /**
   * A quick method to convert simple Write statement to sql update statements for a key-value like schema
   * @param query
   * @return
   */
  public static String convertToSQLUpdate(String query) {
    String sqlUpdate = "";
    
    String[] queryParts = query.split(Constants.QUERY_DELIMETER);
    
    String[] fieldParts = queryParts[1].split(Constants.QUERY_FIELD_DELIMETER);
    // DB_NAME.TABLE_NAME
    //String tableName = fieldParts[0] + Constants.SQL_FIELD_DELIMETER + fieldParts[1];
    String tableName = fieldParts[1];
    // Key is the attribute name. 
    String key = fieldParts[2];
    String keyValue = fieldParts[3];
    // Key Value to select the tuple to be updated
    
    String valueColumn = fieldParts[4];
    String updatedValueField = fieldParts[5];
    
    sqlUpdate = "Update " + tableName + " Set " + valueColumn + " = " + updatedValueField + 
        " where "+ key + " = " + keyValue;
    
    //LOG.debug(sqlUpdate);
    return sqlUpdate;
  }

}
