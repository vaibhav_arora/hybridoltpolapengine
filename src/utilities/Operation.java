package utilities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import transactionalEngine.TransactionalManager;

public class Operation {
  
  String query;
  Set<DataItem> readSet;
  Set<DataItem> writeSet;
  boolean isReadOperation;
  public final static Logger LOG = Logger.getLogger(Operation.class);
  String primaryKey;
  
  public Operation(String query) {
    this.query = query;
    this.readSet = new HashSet<DataItem>();
    this.writeSet = new HashSet<DataItem>();
    //parseReadAndWriteSet(query);
    primaryKey = "";
    parseReadAndWriteSetFromSQL(query);
  }
  
  /**
   * Parsing Function
   * Currently operations are assumed to be of the form
   * Read x y
   * Write x y
   * @param query
   */
  public void parseReadAndWriteSet(String query){
	  LOG.debug("Data Operation:"+query);
	//TODO : Check if analytical queries should be parsed
    String[] queryParts = query.split(Constants.QUERY_DELIMETER);
    if(queryParts.length < 1){
      LOG.error("Empty Operation");
    }
    if(queryParts[0].startsWith(Constants.READ)){
      // Read Operation - Add to read Set
      this.isReadOperation = true;
      for(int i=1; i< queryParts.length; i++){
        DataItem dataItem = new DataItem(queryParts[i]);
        primaryKey += queryParts[i];
        readSet.add(dataItem);
      }
    } else if (queryParts[0].startsWith(Constants.WRITE)){
      //Write Operation - Add to write Set
      for(int i=1; i< queryParts.length; i++){
        DataItem dataItem = new DataItem(queryParts[i]);
        primaryKey += queryParts[i];
        writeSet.add(dataItem);
      }
    } else {
      // Log Error
      LOG.error("Operation doesn't match syntax: "+query);
    }
  }
  
  /**
   * Parsing keys to lock for only simple SQL queries. 
   * Nested queries are not supported
   * @param sqlQuery
   */
  public void parseReadAndWriteSetFromSQL(String sqlQuery){
    LOG.debug("SQL Query:"+ sqlQuery);
    String[] queryTokens = sqlQuery.split(Constants.QUERY_DELIMETER);
    if(queryTokens.length < 1){
      LOG.error("Empty Operation");
    }
    int counter = 1;
    if(queryTokens[0].startsWith(Constants.SELECT)){
      // Select Statement
      this.isReadOperation = true;
      LOG.debug("Select Query");
      boolean lookForKey = false ;
      boolean whereClause = false;
      String key = "";
      int i=1;  
      while(i < queryTokens.length){
        if(lookForKey) {
          // Add this key - Field and value to identify the tuple 
          key += queryTokens[i] + Constants.QUERY_FIELD_DELIMETER + 
              queryTokens[i+2] + Constants.QUERY_FIELD_DELIMETER;
         
          lookForKey = false;
        }
        if(queryTokens[i].startsWith(Constants.WHERE)){
          lookForKey = true;
          whereClause = true;
        } else if(whereClause && queryTokens[i].startsWith(Constants.AND)){
          lookForKey = true;
        }
        i++;
      }
      DataItem dataItem = new DataItem(key);
      if(counter==1){
    	  primaryKey = key;
    	  counter++;
      }
      readSet.add(dataItem);
    } else if(queryTokens[0].startsWith(Constants.UPDATE)) {
      // Update Statement
      // All TPC-C queries update are on a single table
      // lock the key in terms of the predicates of the where clause
      boolean lookForKey = false ;
      boolean whereClause = false;
      String key = "";
      int i=1;
      while(i < queryTokens.length){
        if(lookForKey) {
          // Add this key - Field and value to identify the tuple 
          key += queryTokens[i] + Constants.QUERY_FIELD_DELIMETER + 
              queryTokens[i+2] + Constants.QUERY_FIELD_DELIMETER;
         
          lookForKey = false;
        }
        if(queryTokens[i].startsWith(Constants.WHERE)){
          lookForKey = true;
          whereClause = true;
        } else if(whereClause && queryTokens[i].startsWith(Constants.AND)){
          lookForKey = true;
        }
        i++;
      }
      DataItem dataItem = new DataItem(key);
      if(counter==1){
    	  primaryKey = key;
    	  counter++;
      }
      writeSet.add(dataItem);
    } else if(queryTokens[0].startsWith(Constants.INSERT)){
      // Insert Statement
      //No locks
      DataItem dataItem = new DataItem("No-Locks");
      writeSet.add(dataItem);
    } else {
      // Log Error
      LOG.error("Operation doesn't match syntax: "+sqlQuery);
    }
  }
  
  public Set<DataItem> getReadSet() {
    return this.readSet;
  }
  
  public String getQuery() {
	  return this.query;
  }
  
  public boolean isAReadOperation() {
	  return this.isReadOperation;
  }

  public Set<DataItem> getWriteSet() {
	return this.writeSet;
  }
  
  public String getPrimaryKey() {
    return this.primaryKey;
  }

}
