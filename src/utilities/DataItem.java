package utilities;

public class DataItem {
  
  public String primaryKey;
  public String value;
  
  public DataItem(String primaryKey, String value){
	  this.primaryKey = primaryKey;
	  this.value = value;
  }

  public DataItem(String primaryKey){
    this.primaryKey = primaryKey;
    this.value = null;
  }
  
  public String getPrimaryKey(){
	  return this.primaryKey;
  }
  
  public String getValue(){
	  return this.value;
  }
  
  public void setValue(String value){
	  this.value = value;
  }
}
