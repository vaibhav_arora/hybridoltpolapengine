package utilities;

import java.util.HashMap;
import java.util.Map;

public class DataManagementLayer {
	
	Map<String, DataItem> dataItemMap;
	
	public DataManagementLayer() {
		this.dataItemMap = new HashMap<String, DataItem>();
	}
	
	public DataItem getDataItem(String primaryKey){
		if(!this.dataItemMap.containsKey(primaryKey)){
			return null;
		}
		return this.dataItemMap.get(primaryKey);
	}
	
	public boolean loadDataItem(DataItem dataItem){
		if(this.dataItemMap.containsKey(dataItem.getPrimaryKey())){
			return false;
		}
		this.dataItemMap.put(dataItem.getPrimaryKey(), dataItem);
		return true;
	}

}
