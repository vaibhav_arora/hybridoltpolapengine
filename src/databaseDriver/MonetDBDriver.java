package databaseDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import messaging.HybridEngine.SerializedTransaction;

import org.apache.log4j.Logger;

import transactionalEngine.TransactionalDatabase;
import utilities.Constants;
import utilities.Operation;
import utilities.OperationResult;
import utilities.Transaction;
import utilities.Utils;
import analyticalEngine.AnalyticalDatabase;

public class MonetDBDriver implements AnalyticalDatabase {
  
  private boolean autoCommit;
  private String dbUrl;
  private String user; 
  private String password;
  private long statCount;
  private long statDelay;
  private final static Logger LOG = Logger.getLogger(MonetDBDriver.class);
  
  public MonetDBDriver(String dbUrl, String user, String password, boolean autocommit)
  {
    this.dbUrl = dbUrl;
    this.user = user;
    this.password = password;
    this.autoCommit = autocommit;
    this.statDelay = 0;
    this.statCount = 0;
  }

  @Override
  public OperationResult executeQuery(Operation databaseQuery) {
    Statement stmt = null;
    Connection conn = null;
    OperationResult operationResult = null;
    
    try {
      Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
      conn = DriverManager.getConnection(dbUrl, user, password);
      stmt = conn.createStatement();
      
      String analyticalQuery = databaseQuery.getQuery();
      ResultSet rs = stmt.executeQuery(analyticalQuery);
      
      while(rs.next()){
        String value = rs.getString(Constants.VALUE_COLUMN);    
        System.out.println(value);
      }
      
    } catch (SQLException e) {
      
      LOG.error("SQL Exception", e);
    } catch (ClassNotFoundException e) {
      LOG.error("Class not found Exception", e);
    } finally {
    	try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    
    return operationResult;
  }

  @Override
  public Boolean batchCommit(SerializedTransaction batchedTransaction) {
    Statement stmt = null;
    Connection conn = null;
    OperationResult operationResult = null;
    
    try {
      Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
      conn = DriverManager.getConnection(dbUrl, user, password);
      conn.setAutoCommit(autoCommit);
      stmt = conn.createStatement();
      
      for(String updateStmt : batchedTransaction.getUpdateOperationList()){
    	String updateSQLstatement = Utils.convertToSQLUpdate(updateStmt);
        stmt.addBatch(updateSQLstatement);
        LOG.debug("Update Statement:"+updateSQLstatement);
      }
      
      stmt.executeBatch();
      conn.commit();
      this.statDelay += System.currentTimeMillis() - batchedTransaction.getOperationCommitTime();
      this.statCount++;
      long avgDelay = this.statDelay / this.statCount;
      LOG.info("Average delay is: "+avgDelay);
      
    } catch (SQLException e) {
      try {
		conn.close();
	  } catch (SQLException e1) {
		e1.printStackTrace();
	  }
      LOG.error("SQL Exception", e);
      return false;
    } catch(ClassNotFoundException e) {
      LOG.error("ClassNotFoundException", e);
      return false;
    } finally {
    	try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    return true; 
  }
  
}
