package databaseDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import transactionalEngine.TransactionalDatabase;
import utilities.Constants;
import utilities.Operation;
import utilities.OperationResult;
import utilities.Transaction;
import utilities.Utils;

public class MySQLDriver implements TransactionalDatabase {

	private Map<Long, Connection> connectionPool;
	private final static Logger LOG = Logger.getLogger(MySQLDriver.class);
	private boolean autoCommit;
	private String dbUrl;
	private String user; 
	private String password;
	
	
	public MySQLDriver(String dbUrl, String user, String password, 
	    boolean autocommit) {
		this.connectionPool = new HashMap<Long, Connection>();
		this.dbUrl = dbUrl;
		this.user = user;
		this.password = password;
		this.autoCommit = autocommit;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			LOG.error("Error in loading the driver");
			e.printStackTrace();
		}
	}
	
	@Override
	public void executeQuery(Long transactionId, Operation databaseQuery) {
		Statement stmt = null;
		Connection conn = getConnectionInstance(transactionId);
		String query = databaseQuery.getQuery();
		
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ResultSet rs = stmt.executeQuery(query);
			//Process the Result set and return the results
			//TODO : change the return type from the boolean to OperationResult
			while(rs.next()){
				String value = rs.getString(Constants.VALUE_COLUMN);		
				LOG.debug(value);
			}	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public boolean processCommit(Long transactionId) {
		
        Statement stmt = null;
        Connection conn = getConnectionInstance(transactionId);
        
		try {
			stmt = conn.createStatement();
			//Commit the Transaction associated with the connection
			if(!this.autoCommit){
				conn.commit();
			}
			//Close the connection after committing
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return false;
	}

	@Override
	public boolean processReadRequest(Long transactionId,
			Operation readOperation) {
		Statement stmt = null;
		Connection conn = getConnectionInstance(transactionId);
		String readStatement = readOperation.getQuery();
		String resultSetString = "";
		
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
		    //TODO : Remove the next line when SQL queries are supported
			//TODO : and uncomment for experiments with Read and Write statements
		    //readStatement = Utils.convertToSQLSelect(readStatement);
			ResultSet rs = stmt.executeQuery(readStatement);
			//Process the Result set and return the results
			//TODO : change the return type from the boolean to OperationResult
			//TODO: Extracting the table name from the query rather than hard coding it
			while(rs.next()){
				ResultSetMetaData rsmd = rs.getMetaData();
				int numColumns = rsmd.getColumnCount();
				for(int i=1; i <= numColumns; i++){
					resultSetString += rs.getString(i) + Constants.
							RESULT_COLUMN_DELIMITER;
				}
				resultSetString += Constants.RESULT_ROW_DELIMITER;
			  //Un comment for experiment with read and write statements
			  //String value = rs.getString(Constants.VALUE_COLUMN);		
			}
			LOG.debug("Result string:"+resultSetString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean processWriteRequest(Long transactionId,
			Operation writeOperation) {
		
		Statement stmt = null;
		Connection conn = getConnectionInstance(transactionId);
		String writeStatement = writeOperation.getQuery();
		
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
		  //TODO : Remove the next line when SQL queries are supported
			//TODO : and comment for experiments with Read and Write statements
			//writeStatement = Utils.convertToSQLUpdate(writeStatement);
			int numberOfRowsEffected = stmt.executeUpdate(writeStatement);
			//Process the Result set and return the results
			//LOG.debug("No of rows effected:"+numberOfRowsEffected);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} 
	}
	
	/**
	 *  Batch commit all changes of a single transaction
	 */
	@Override
	public boolean batchCommit(Transaction transaction) {
	    Statement stmt = null;
	    Connection conn = null;
	    
	    try {
	      conn = getConnectionInstance(transaction.getTransactionId());
	      stmt = conn.createStatement();
	      conn.setAutoCommit(autoCommit);
	      for(Operation updateOperation : transaction.getWriteOperations()){
	       	//String updateSQLstatement = Utils.convertToSQLUpdate(updateOperation.getQuery());
	        //TODO : Uncomment above and Comment the line below 
	        //for experiments with Read and Write statements
	        String updateSQLstatement = updateOperation.getQuery();
	        stmt.addBatch(updateSQLstatement);
	        LOG.debug("Update Statement:"+updateSQLstatement);
	      }
	         
	      stmt.executeBatch();
	      if(!this.autoCommit){
	        conn.commit();
	      }
	     
	    	
	    }  catch (SQLException e) {
	        
	        LOG.error("SQL Exception:"+e.getMessage(), e);
	        return false;
	    } finally {
	    	closeConnectionInstance(transaction.getTransactionId());
	    }
	    
	    return true;
	}
	
	/**
	 * Returns the connection attached to a particular transaction 
	 * @param transactionId
	 * @return
	 */
	private Connection getConnectionInstance(Long transactionId){
		
		if(!connectionPool.containsKey(transactionId)){
			Connection conn = null;
			try {
				//TODO : add Username and password if a user is added
				//conn = DriverManager.getConnection(dbUrl, user, password);
				conn = DriverManager.getConnection(dbUrl);
				conn.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				//LOG.error("Could not connect it to the database");
				e.printStackTrace();
			}
			connectionPool.put(transactionId, conn);
		}
		return connectionPool.get(transactionId);
	}
	
	public void closeConnectionInstance(Long transactionId){
		
		if(!connectionPool.containsKey(transactionId)){
			LOG.error("No coonection associated with transactionId:"+transactionId);
			return;
		}
		try {
			connectionPool.get(transactionId).close();
		} catch (SQLException e) {
			LOG.info("Problems in closing the connection for transactionId:"+transactionId);
			e.printStackTrace();
		}	
	}

}
