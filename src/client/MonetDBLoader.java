package client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class MonetDBLoader {
	
	 private final static Logger LOG = Logger.getLogger(MonetDBLoader.class);
	 private boolean autoCommit;
	 private String dbUrl;
	 private String user; 
	 private String password;
	  
	 public MonetDBLoader() {
	   dbUrl = "jdbc:monetdb://127.0.0.1/test";
	   user = "monetdb";
	   password = "monetdb";
	   autoCommit = false;
	   
	   try {
		 Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
	   } catch (ClassNotFoundException e) {
		 e.printStackTrace();
	   }
	 }
	  
	  /**
	   * Assumes a schema with (key, value) - (int, String)
	   * @param tableName
	   * @param numItems
	   */
	  public void loadItems(String tableName, int numItems) {
	    String insertSQLStatement;
	    Connection conn;
	    
	    try {
	      conn = DriverManager.getConnection(dbUrl, user, password);
	      conn.setAutoCommit(autoCommit);
	      Statement stmt = conn.createStatement();
	      
	      for(int i=1000000; i < numItems; i++){
	        insertSQLStatement = "Insert into "+tableName+" values("+i+
	            ", 'Value"+i+"')";
	        stmt.addBatch(insertSQLStatement);
	      }
	      
	      stmt.executeBatch();
	      conn.commit();
	    } catch (SQLException e) {
	      LOG.error("Exception occured ", e);
	    }
	  }
	  
	  public static void main(String[] args) {
		MonetDBLoader moentDBLoader = new MonetDBLoader();
	    int numItemsToLoad = 2000000;
	    String tableName = "TestTable";
	    // Load Items into the table
	    moentDBLoader.loadItems(tableName, numItemsToLoad);
	    
	  }

}
