package client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.mockito.Mockito;

import databaseDriver.MySQLDriver;

import rpc.RPCClient;
import transactionalEngine.LockBasedConcurrencyScheme;
import transactionalEngine.LockingConcurrencySchemeTest;
import transactionalEngine.TransactionBatchProcessor;
import transactionalEngine.TransactionalDatabase;
import transactionalEngine.TransactionalManager;
import utilities.Constants;
import utilities.Operation;
import utilities.TransactionBatch;

public class TransactionalEngineClient {
	
	LockBasedConcurrencyScheme lockBasedConcurrencyScheme;
	public static TransactionalDatabase transactionalDatabase;
	
	TransactionBatchProcessor transactionBatchProcessor;
	
	TransactionalManager transactionalManager;
	Random rnGenerator;
	public BufferedWriter bw;
	int clientsDone;
	long timeFinished;
	long totalCommits;
	RPCClient analyticalEngineRPCClient;
	
	private final static Logger LOG = Logger.getLogger(LockingConcurrencySchemeTest.class);
	
	public TransactionalEngineClient() {
		//transactionalDatabase = Mockito.mock(TransactionalDatabase.class);
		
		String dbUrl = "jdbc:mysql://127.0.0.1:3306/test";

		// currently just connect to the localhost default user with no password
		String user = "";
		String password = "";
		boolean autocommit = false;
		MySQLDriver mySQLDriver = new MySQLDriver(dbUrl, user, password, autocommit);
		
		transactionBatchProcessor = Mockito.mock(TransactionBatchProcessor.class);
		int partitionID = 1;
		int initBatchID = 1;
		analyticalEngineRPCClient = Mockito.mock(RPCClient.class);
		//transactionBatchProcessor = new TransactionBatchProcessor(analyticalEngineRPCClient, 
		//		initBatchID, partitionID);
		TransactionBatch transactionBatch;
		lockBasedConcurrencyScheme = new LockBasedConcurrencyScheme(mySQLDriver); 
		
		transactionalManager = new TransactionalManager(
				lockBasedConcurrencyScheme, transactionBatchProcessor);
		
		rnGenerator = new Random();
		clientsDone = 0;
		totalCommits = 0;
		
		String fileName = "resources/testRuns/"+"output.txt" ;
		bw = null;
		try {
	      File outFile = new File(fileName);
		  bw = new BufferedWriter(new FileWriter(outFile));
		} catch (FileNotFoundException e) {
		  e.printStackTrace();
		} catch (IOException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		}
	}
	

	public class ClientRun implements Runnable{
		int clientId;
		int numTransactions;
		int numOperations;
		int numReads;
		int numItems;
		
		public ClientRun(Integer clientId, int numTransactions, int numItems, 
				int numOperations, int numReads) {
			this.clientId = clientId;
			this.numTransactions = numTransactions;
			this.numOperations = numOperations;
			this.numReads = numReads;
			this.numItems = numItems;
		}
		@Override
		public void run() {
			// Process the test file and send requests to the locking manager after parsing them
			try{
			  // Creating 2 transactions for the simple test to see that the requests 
			  // can't get conflicting requests	
			  
			  // Each client 	
			  int numCommits = 0;
				
			  for(int i=0; i < numTransactions; i++){
				// For each transaction  numOperations are generated
				
				long transactionId = numTransactions * clientId + i;
				//Begin Request
				transactionalManager.beginTransaction(transactionId);
				
				// First send numReads read operations
			    for(int j=0; j < numReads; j++) {
			    	String readQueryPrefix = Constants.READ + Constants.QUERY_DELIMETER;
			    	String primaryKeyPrefix = "test#TestTable#table_key#" ; 
			    	
			    	int itemToRead = rnGenerator.nextInt(numItems)+1;
			    	String keyToRead = primaryKeyPrefix + itemToRead;
			    	
			    	String readQuery = readQueryPrefix + keyToRead;
			    	
			    	Operation readOperation = new Operation(readQuery);
			        transactionalManager.processReadRequest(transactionId, readOperation);	
			    }
			    // Then send write operations 
			    int numWrites = numOperations - numReads;
                for(int j=0; j < numWrites; j++) {
                  String writeQueryPrefix = Constants.WRITE + Constants.QUERY_DELIMETER;
			      String primaryKeyPrefix = "test#TestTable#table_key#" ; 
			    	
			      int itemToWrite = rnGenerator.nextInt(numItems)+1;
			      String keyToWrite = primaryKeyPrefix + itemToWrite;
			      String valueToWrite = "Value" + rnGenerator.nextInt(numItems);
			    	
			      String writeQuery = writeQueryPrefix + keyToWrite + Constants.QUERY_FIELD_DELIMETER +
			    		  "value" + Constants.QUERY_FIELD_DELIMETER + "'" + valueToWrite + "'";
			    	
			      Operation writeOperation = new Operation(writeQuery);
			      transactionalManager.processWriteRequest(transactionId, writeOperation);		    	
			    }
                
                //Then send a commit operation
                Boolean didCommit = transactionalManager.processCommitRequest(transactionId);
                if(didCommit) {
                	numCommits++;
                }
			  }			  
			  
			  // Write commits to file
			  writeCommitsToFile(clientId, numCommits);
			  long timeFinished = System.currentTimeMillis();
			  registerEndOfRun(clientId, numCommits, timeFinished);
			} catch(Exception e) {
				LOG.error("Caught exception",e);
			}		
		}
	}
	
	public void writeCommitsToFile(int clientId, int numCommits) {
		try {
			bw.write("Client Id:"+clientId+" "+numCommits+"\n");
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
	public void registerEndOfRun(int clientId, int numCommits, long timeFinished) {
		clientsDone++;
		totalCommits += numCommits; 
		this.timeFinished = Math.max(timeFinished, this.timeFinished);
	}
	
	public int getClientsDone() {
		return this.clientsDone;
	}
	
	public long getTimeFinished() {
		return this.timeFinished;
	}
	
	public long getTotalCommits() {
		return this.totalCommits;
	}
	
	public static void main(String[] args){
		TransactionalEngineClient transactionalEngineClient = new TransactionalEngineClient();
		
		
		int numclients = 10;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(numclients+10);
		int numTransactions = 100;
		int numOperations = 5;
		int numReads = 3;
		int numItems = 1000;
		
		taskExecutor.execute(transactionalEngineClient.lockBasedConcurrencyScheme);
		//taskExecutor.execute(transactionalEngineClient.transactionBatchProcessor);
		long startTime = System.currentTimeMillis();
		for(int i= 0 ; i < numclients; i++ ){
			taskExecutor.execute(transactionalEngineClient.new ClientRun(i, numTransactions
					, numItems, numOperations, numReads));	
		}
		
		while(true){
			if(transactionalEngineClient.clientsDone == numclients){
				try {
					transactionalEngineClient.bw.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				System.out.println("All clients are done");
				long experimentDuration = transactionalEngineClient.timeFinished - 
						startTime;
				
				System.out.println("Experiment duration: "+experimentDuration+"ms Total Commits: "
						+ transactionalEngineClient.getTotalCommits());
				break;
			}
			try {
				Thread.sleep(100L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
