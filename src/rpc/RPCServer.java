package rpc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import messaging.HybridEngine.HybridStoreService;

import com.googlecode.protobuf.socketrpc.RpcServer;
import com.googlecode.protobuf.socketrpc.ServerRpcConnectionFactory;
import com.googlecode.protobuf.socketrpc.SocketRpcConnectionFactories;

public class RPCServer {
	
	RpcServer server;
	ServerRpcConnectionFactory rpcConnectionFactory;
	ExecutorService threadPool;
	
	public RPCServer(HybridStoreService service, int port) {
		rpcConnectionFactory = SocketRpcConnectionFactories
			    .createServerRpcConnectionFactory(port);
		threadPool = Executors.newCachedThreadPool();
		server = new RpcServer(rpcConnectionFactory, 
			    threadPool, true);
		
		server.registerService(service);
	}
	
	public void start() {
		server.startServer();
	}
	
	public void stop() {
		server.shutDown();
	}

}
