package transactionalEngine;

import utilities.Operation;
import utilities.Transaction;

public interface ConcurrencyControlScheme {
	
	public boolean processReadRequest(Transaction transaction, Operation readOperation);
	
	public boolean processWriteRequest(Transaction transaction, Operation writeOperation);
	
	public boolean processCommitRequest(Transaction transaction);

}
