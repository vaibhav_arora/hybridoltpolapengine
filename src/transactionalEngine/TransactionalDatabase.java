package transactionalEngine;

import utilities.Operation;
import utilities.Transaction;

public interface TransactionalDatabase {
  
  public void executeQuery(Long transactionId, Operation databaseQuery);
  
  public boolean processCommit(Long transactionId);
  
  public boolean processReadRequest(Long transactionId, Operation readOperation);
  
  public boolean processWriteRequest(Long transactionId, Operation writeOperation);
  
  public boolean batchCommit(Transaction transaction);

}
