package transactionalEngine;

import java.util.HashMap;
import java.util.Map;

import messaging.HybridEngine.HybridStoreService.BlockingInterface;
import messaging.HybridEngine.SerializedTransaction;
import messaging.HybridEngine.TransactionalBatchRequest;
import messaging.HybridEngine.TransactionalBatchRequestOrBuilder;
import messaging.HybridEngine.TransactionalBatchResponse;

import org.apache.log4j.Logger;

import rpc.RPCClient;
import utilities.Constants;
import utilities.Transaction;
import utilities.TransactionBatch;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.googlecode.protobuf.socketrpc.SocketRpcController;

/**
 * Runs periodically and sends the batch of changes from the row store
 * to the column store for ingestion
 * @author vaibhav
 *
 */
public class TransactionBatchProcessor implements Runnable{

  RPCClient analyticalEngineRPCClient;
  Map<Integer, TransactionBatch> closedBatches;
  TransactionBatch currentTransactionBatch;
  Integer currentBatchId;
  // The id of the partition the batch processor is running at
  int partitionID;
  public final static Logger LOG = Logger.getLogger(TransactionBatchProcessor.class);
  
  public TransactionBatchProcessor(RPCClient analyticalEngineRPCClient, int initBatchId, int partitionID) {
	  this.analyticalEngineRPCClient = analyticalEngineRPCClient;
	  // initBatchId is the initial batch Id to which transactions should be added after a system start up.
	  // On a crash, recovery component finds out the initial batch Id by going through the 
	  // transaction Logs (Hybrid OLTP OLAP Paper)
	  currentBatchId = initBatchId;
	  this.partitionID = partitionID;
	  currentTransactionBatch = new TransactionBatch(initBatchId);
	  // The batches will be shipped and processed in order so keeping them as 
	  // lists might help too
	  this.closedBatches = new HashMap<Integer, TransactionBatch>();
  }
	
  @Override
  public void run() {
    
	  // Batch Processor Runs as a periodic Process
	  while(true) {
		  if(this.currentTransactionBatch.getTransactionList().size() > 0){
			  LOG.info("Periodic Check. BatchId:"+ this.currentBatchId +" Batch Size:"+
				      this.currentTransactionBatch.getTransactionList().size());
		  }
		  
		  
		  // check the size of the current transaction Batch
		  // For the first cut, threshold of sending over a batch to analytical engine is only based on time
		  // Check the size of the transaction batch and if it is greater than 1, we send it
		 
		  if(this.currentTransactionBatch.getTransactionList().size() >= 1){
			  int batchIDClosed = currentBatchId;
			  TransactionBatch closedTransactionBatch = null;
			  
			  synchronized (this) {
				  //TODO: Check for concurrent issues if a thread is adding a transaction to the current batch at the same time
				  //Close the batch and make the next batch active
				  
				  closedBatches.put(batchIDClosed, currentTransactionBatch);
				  //TODO: Also determine when to remove the closed batches from the map. One solution is 
				  // to remove them as soon as their ingestion/receipt is acknowledged
				
				  closedTransactionBatch = currentTransactionBatch;
				  // TODO : Write the Closure of the batch with batchId:'batchIDClosed' to the log
				  
				  currentBatchId++;
				  currentTransactionBatch = new TransactionBatch(currentBatchId);	
			  }
			
			  // Send the batch across to the Analytical Engine
			  // Batch version numbers ensure the exactly one semantics of batch ingestion
			  LOG.debug("Sending transactional batch to analytical engine with Id:"+
					  batchIDClosed);
			  sendBatchToAnalyticalEngine(closedTransactionBatch);
		  }
		  
		  try {
			Thread.sleep(Constants.BATCH_SENDING_PERIOD);
		  } catch (InterruptedException e) {
			  e.printStackTrace();
		  }
	  }
    
  }
  
  public void addToCurrentBatch(Transaction transaction) {
	  // TODO : Check if this block needs to be synchronized
	  LOG.debug("Adding to the batchId:"+currentBatchId+" transaction with Id:"+
	  transaction.getTransactionId());
	  this.currentTransactionBatch.addTransaction(transaction);
  }
  
  public TransactionBatch getCurrentTransactionBatch() {
	  return this.currentTransactionBatch;
  }
  
  public void sendBatchToAnalyticalEngine(TransactionBatch transactionBatch) {
	// Invoke the service and the controller for the call
	//TODO: Call the blocking client for Analytical Engine 
	BlockingInterface service = analyticalEngineRPCClient.getClientBlockingInterface();
	RpcController controller = new SocketRpcController();
	
	// TODO: Set the transaction Batch List
	TransactionalBatchRequest.Builder transactionalBatchRequestBuilder = TransactionalBatchRequest.newBuilder().
			setBatchId(transactionBatch.getBatchId()).setPartionId(this.partitionID).
			setWaitToIngest(transactionBatch.getHasDistributedtransactions());
	
	for(SerializedTransaction serializedTransaction: transactionBatch.getTransactionList()) {
		if(serializedTransaction != null){
		  transactionalBatchRequestBuilder.addTransactionList(serializedTransaction);
		}
	}
	
	try {
		if(service != null){
			TransactionalBatchResponse transactionBatchResponse = service.
					transferTransactionalBatch(controller, transactionalBatchRequestBuilder.build());
		} else {
			LOG.error("Service is not instantiated correctly");
		}
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		LOG.error("Service error", e);
	}
	
	//TODO: Process if the request was successful 
  }

}
