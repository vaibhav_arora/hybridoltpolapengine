package transactionalEngine;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import utilities.Constants;
import utilities.Operation;
import utilities.OperationResult;
import utilities.Transaction;
import utilities.TransactionState;

public class TransactionalManager {
  
  Map<Long, Transaction> transactionMap;
  ConcurrencyControlScheme concurrencyControlScheme;
  TransactionBatchProcessor transactionBatchProcessor;
  public final static Logger LOG = Logger.getLogger(TransactionalManager.class);
  
  public TransactionalManager(ConcurrencyControlScheme concurrencyControlScheme, TransactionBatchProcessor
		   transactionBatchProcessor){
    this.transactionMap = new  HashMap<Long, Transaction>();
    this.concurrencyControlScheme = concurrencyControlScheme;
    this.transactionBatchProcessor = transactionBatchProcessor;
  }
  
  public boolean beginTransaction(Long transactionId){
    LOG.debug("Transactional Begin Request with transactionId:"+transactionId);
    
    if(this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id already exists");
      throw new RuntimeException("Transaction Id already exists");
    }
    LOG.debug("Invoking a transaction with Id: "+transactionId);
    Transaction transaction = new Transaction(transactionId);
    this.transactionMap.put(transactionId, transaction);
    return true;
  }
  
  public OperationResult processReadRequest(Long transactionId, Operation 
      readOperation) {
	  
    LOG.debug("Transactional Read Request for transactionId:"+transactionId);
    if(!this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id does not exist:"+transactionId);
      throw new RuntimeException("Transaction Id does not exist");
    }
    
    OperationResult readResult = new OperationResult();
    
    Transaction readTransaction = transactionMap.get(transactionId);
    if(readTransaction.getState().equals(TransactionState.ABORTED)){
    	LOG.error("Transaction has already been aborted");
    	readResult.setOperationStatus(false);
    	return readResult;
    }
  
    
    // Here the processing has to be based on concurrency control
    // If it is occ then get the last transaction Read from dataManagement Layer
    // Otherwise if it is pessimistic concurrency control get a read lock on the item
    Boolean operationStatus = concurrencyControlScheme.processReadRequest(readTransaction, 
			  readOperation);
    
    readResult.setOperationStatus(operationStatus);
    if(!operationStatus){
    	readTransaction.setState(TransactionState.ABORTED);
    } else {
      // Add the operation to the read and write set
      readTransaction.addReadOperation(readOperation);
    }
    return readResult;
  }
  
  public OperationResult processWriteRequest(Long transactionId, Operation 
      writeOperation){
    LOG.debug("Transactional Write Request for transactionId:"+transactionId);
    if(!this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id does not exist:"+transactionId);
      throw new RuntimeException("Transaction Id does not exist");
    }
    OperationResult writeResult = new OperationResult();
	 
    Transaction writeTransaction = transactionMap.get(transactionId);
    if(writeTransaction.getState().equals(TransactionState.ABORTED)){
    	LOG.error("Transaction has already been aborted");
    	writeResult.setOperationStatus(false);
    	return writeResult;
    }
    
    
    
    // Here the processing has to be based on concurrency control
    // If it is occ then get the last transaction Write on this item from dataManagement Layer
    // Otherwise if it is pessimistic concurrency control get a write lock on the item
    
    Boolean operationStatus = concurrencyControlScheme.processWriteRequest(writeTransaction, 
			  writeOperation);
    writeResult.setOperationStatus(operationStatus);
	//Abort the transaction if status of the operation is false  
    if(!operationStatus){
    	writeTransaction.setState(TransactionState.ABORTED);
    }  else {
      // Add the operation to the read and write set
      //TODO Hack - Remove later
      if(!writeOperation.getQuery().startsWith(Constants.INSERT)) {
        writeTransaction.addWriteOperation(writeOperation);
      }      
    }
    
    return writeResult;
  }
  
  
  public boolean processCommitRequest(Long transactionId){
    LOG.debug("Transactional Commit Request for transactionId:"+transactionId);
    if(!this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id does not exist:"+transactionId);
      throw new RuntimeException("Transaction Id does not exist");
    }
    
    Transaction transactionWithCommitRequest = transactionMap.get(transactionId);
    if(transactionWithCommitRequest.getState().equals(TransactionState.ABORTED)){
    	LOG.error("Transaction is already aborted");
    	return false;
    }
    
    Boolean commitStatus = concurrencyControlScheme.processCommitRequest(transactionWithCommitRequest);
    
    // If commit is successful, add the transaction to the batch to be sent over
    if(commitStatus){
    	 transactionWithCommitRequest.setOperationCommitTime(System.currentTimeMillis());
    	 this.transactionBatchProcessor.addToCurrentBatch(transactionWithCommitRequest);
    }
    return commitStatus;
  }

}
