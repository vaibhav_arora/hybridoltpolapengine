package transactionalEngine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import databaseDriver.MySQLDriver;

import utilities.DataItem;
import utilities.Operation;
import utilities.Transaction;

public class LockBasedConcurrencyScheme implements ConcurrencyControlScheme, Runnable {
	
	TransactionalDatabase transactionDataBase;
	ConcurrentHashMap<String, List<Long>> readLocks;
	// Since write locks are not shared. We maintain a mapping from the item to the transaction Id
	ConcurrentHashMap<String, Long> writeLocks;
	ConcurrentHashMap<String, ConcurrentLinkedQueue<WaitLockRequest>> readLockQueue;
	//TODO: Check if concurrent linked queue should be used
	ConcurrentHashMap<String, ConcurrentLinkedQueue<WaitLockRequest>> writeLockQueue;
	//ConcurrentHashMap<String, List<Long>> writeLockQueue;
	ConcurrentLinkedQueue<String> readLocksReleased;
	ConcurrentLinkedQueue<String> writeLocksReleased;
	public final static Logger LOG = Logger.getLogger(LockBasedConcurrencyScheme.class);
	public static long BACKGROUND_WAIT = 2L;
	// Waiting period after which transactional read or write operation is deemed unsuccessful 
	// and the transaction aborts
	public static long LOCK_TIMEOUT_PERIOD = 200L;
	
	public LockBasedConcurrencyScheme(TransactionalDatabase transactionDataBase) {
		this.transactionDataBase = transactionDataBase;
		this.readLocks = new ConcurrentHashMap<String, List<Long>>();
		this.writeLocks = new ConcurrentHashMap<String, Long>();
		this.readLockQueue = new ConcurrentHashMap<String,ConcurrentLinkedQueue<WaitLockRequest>>();
		this.writeLockQueue = new ConcurrentHashMap<String, ConcurrentLinkedQueue<WaitLockRequest>>();
		this.readLocksReleased = new ConcurrentLinkedQueue<String>();
		this.writeLocksReleased = new ConcurrentLinkedQueue<String>();
	}
	
	@Override
	public boolean processReadRequest(Transaction transaction, Operation readOperation) {
		// TODO Get the read lock for the item
		// If you can't get the lock put the item in a queue waiting for the read lock
		
		//Note: Each item is read in a specific read request 
		// Leaving the operation as generic if to used to when read/write sets are defined in advanced
		
		String primaryKey = null;
		Iterator<DataItem> it = readOperation.getReadSet().iterator();
		while(it.hasNext()){
			primaryKey = it.next().getPrimaryKey();
		}
		
		// Get a read lock for the item
		//TODO: If you can't get the lock wait till the timeout to get the lock
				
		if(!this.writeLocks.containsKey(primaryKey)){
			// Acquire read lock on the item
			synchronized (this) {
				if(!this.writeLocks.containsKey(primaryKey)){
					LOG.debug("Granted read lock on key "+primaryKey+" to transactionId:"+transaction.getTransactionId());
					this.readLocks.putIfAbsent(primaryKey, new ArrayList<Long>());
					this.readLocks.get(primaryKey).add(transaction.getTransactionId());
				} else {
				  //TODO:  Reformat Later	
					
				  //TODO: Can't acquire the lock
				  // Add lock request to the Queue
				  this.readLockQueue.putIfAbsent(primaryKey, new ConcurrentLinkedQueue<WaitLockRequest>());
				  // Add transaction to the queue
				  WaitLockRequest waitLockRequest = new WaitLockRequest(transaction.getTransactionId(), Thread.currentThread());
				  this.readLockQueue.get(primaryKey).add(waitLockRequest);
				  //Wait for the read lock until you timeout
				  
				  while(true){
						// If you get the lock, then break
						
						// Check if you waited for the timeout period, then return false
						// and the transaction will abort. Also, have to release the other locks the transaction
						// might have
					  
					   try {
							Thread.sleep(LOCK_TIMEOUT_PERIOD);
						  } catch (InterruptedException e) {
							// Since the thread was interrupted, it means the lock was granted
							LOG.info("Thread was interuppted because the transaction acquired the read lock");
							break;
						  }
			        	  // Not able to get the locks, hence return false and release other locks held by a transaction
					      LOG.info("Not able to get the read lock for key:"+primaryKey+" Hence aborting");
					      //Remove the request from the waiting queue
					      this.readLockQueue.get(primaryKey).remove(waitLockRequest);
					      // Release the locks held
			        	  releaseLocks(transaction);
					      return false;
					}
				}
			}
		
		} else {
			//TODO: Can't acquire the lock
			// Add lock request to the Queue
		  this.readLockQueue.putIfAbsent(primaryKey, new ConcurrentLinkedQueue<WaitLockRequest>());
		  // Add transaction to the queue
		  WaitLockRequest waitLockRequest = new WaitLockRequest(transaction.getTransactionId(), Thread.currentThread());
		  this.readLockQueue.get(primaryKey).add(waitLockRequest);
		  //Wait for the read lock until you timeout
		  
		  while(true){
				// If you get the lock, then break
				
				// Check if you waited for the timeout period, then return false
				// and the transaction will abort. Also, have to release the other locks the transaction
				// might have
			  
			   try {
					Thread.sleep(LOCK_TIMEOUT_PERIOD);
				  } catch (InterruptedException e) {
					// Since the thread was interrupted, it means the lock was granted
					LOG.info("Thread was interuppted because the transaction acquired the read lock");
					break;
				  }
	        	  // Not able to get the locks, hence return false and release other locks held by a transaction
			      LOG.info("Not able to get the read lock for key:"+primaryKey+" Hence aborting");
			      //Remove the request from the waiting queue
			      this.readLockQueue.get(primaryKey).remove(waitLockRequest);
			      // Release the locks held
	        	  releaseLocks(transaction);
	        	  return false;
			}
		}
		
		
		// Read the items from the database layer
		transactionDataBase.processReadRequest(transaction.getTransactionId(), readOperation);
		return true;
	}

	@Override
	public boolean processWriteRequest(Transaction transaction,
			Operation writeOperation) {
		// TODO Get the write lock on the item
		// If we can't get the write lock put the item in a queue waiting for 
		// the write lock
		
		//Note: Each item is read in a specific read request 
		// Leaving the operation as generic if to used to when read/write sets are defined in advanced
				
		String primaryKey = null;
		Iterator<DataItem> it = writeOperation.getWriteSet().iterator();
		while(it.hasNext()){
		  primaryKey = it.next().getPrimaryKey();
		}
		
		if(primaryKey.startsWith("No-Locks")){
		  // Don't execute the procedure to lock. This is a Tpcc Insert statement
		  transactionDataBase.processWriteRequest(transaction.getTransactionId(), writeOperation);
		  return true;
		}
		//Get a lock for each item in the write set
		// any other write lock should not exist
		// Also, either there shouldn't be any read lock or the transaction should have an exclusive read lock
		if(!this.writeLocks.containsKey(primaryKey) && (!this.readLocks.containsKey(primaryKey) || (
				this.readLocks.get(primaryKey).size() == 1 && this.readLocks.get(primaryKey).get(0) == transaction.getTransactionId()))){
			// Acquire a write lock on the item
			// Using the put if absent to guarantee the action of getting the lock is atomic
			synchronized (this) {
				if(!this.writeLocks.containsKey(primaryKey) && (!this.readLocks.containsKey(primaryKey) || (
						this.readLocks.get(primaryKey).size() == 1 && this.readLocks.get(primaryKey).get(0) == transaction.getTransactionId()))){
					this.writeLocks.putIfAbsent(primaryKey, transaction.getTransactionId());
					LOG.debug("Granted write lock on key "+primaryKey + "to transactionId:"+transaction.getTransactionId());
				} else {
					
					//TODO: Reformat Later
					
					//TODO: Can't acquire the lock
					//Add lock request to the queue
				  this.writeLockQueue.putIfAbsent(primaryKey, new ConcurrentLinkedQueue<WaitLockRequest>());
				  // Add transaction to the queue
				  WaitLockRequest waitLockRequest = new WaitLockRequest(transaction.getTransactionId(), Thread.currentThread());
		          this.writeLockQueue.get(primaryKey).add(waitLockRequest);
		          //Wait for the write lock until you timeout
		          while(true){
						// If you get the lock, then break
					  
						// Check if you waited for the timeout period, then return false
						// and the transaction will abort. Also, have to release the other locks the transaction
						// might have
		        	  
		        	  try {
						Thread.sleep(LOCK_TIMEOUT_PERIOD);
					  } catch (InterruptedException e) {
						// Since the thread was interrupted, it means the lock was granted
						LOG.info("Thread was interuppted because the transaction acquired the write lock");
						break;
					  }
		        	  // Not able to get the locks, hence return false and release other
		        	  //locks held by a transaction
		        	  LOG.info("Not able to get the write lock for key:"+primaryKey+" Hence aborting");
		        	  //Remove the request from the waiting queue
				      this.writeLockQueue.get(primaryKey).remove(waitLockRequest);
		        	  //Release the locks held
		        	  releaseLocks(transaction);
		        	  return false;
					}
				}
			}
			
		} else {
			//TODO: Can't acquire the lock
			//Add lock request to the queue
		  this.writeLockQueue.putIfAbsent(primaryKey, new ConcurrentLinkedQueue<WaitLockRequest>());
		  // Add transaction to the queue
		  WaitLockRequest waitLockRequest = new WaitLockRequest(transaction.getTransactionId(), Thread.currentThread());
          this.writeLockQueue.get(primaryKey).add(waitLockRequest);
          //Wait for the write lock until you timeout
          while(true){
				// If you get the lock, then break
			  
				// Check if you waited for the timeout period, then return false
				// and the transaction will abort. Also, have to release the other locks the transaction
				// might have
        	  
        	  try {
				Thread.sleep(LOCK_TIMEOUT_PERIOD);
			  } catch (InterruptedException e) {
				// Since the thread was interrupted, it means the lock was granted
				LOG.info("Thread was interuppted because the transaction acquired the write lock");
				break;
			  }
        	  // Not able to get the locks, hence return false and release other
        	  //locks held by a transaction
        	  LOG.info("Not able to get the write lock for key:"+primaryKey+" Hence aborting");
        	  //Remove the request from the waiting queue
		      this.writeLockQueue.get(primaryKey).remove(waitLockRequest);
        	  // Release the locks held
        	  releaseLocks(transaction);
        	  return false;
			}
		}
			
		// Write the item to the database layer
		//Writes are flushed on commit
		//transactionDataBase.processWriteRequest(transaction.getTransactionId(), writeOperation);
		return true;
	}

	@Override
	public boolean processCommitRequest(Transaction transaction) {
		// TODO If the transaction has all the locks
		// (A commit request should only come when it has acquired all of them)
		// then commit the transaction and release the locks
		
		// Actually write the items to the database
		//TODO: Check for correctness - Should we write the transaction Log and operation to the batch
		// before Flushing the records ? 
		
		// Commenting this and changing to a batch commit
		/*
		for(Operation writeOperation: transaction.getWriteOperations()) {
			// Write the buffered operations to the database
			transactionDataBase.processWriteRequest(transaction.getTransactionId(), 
			    writeOperation);
		}
		
		transactionDataBase.processCommit(transaction.getTransactionId());
		*/
		// Flushing all the write operations to MySQL at once
		boolean commitstatus = transactionDataBase.batchCommit(transaction);
		
		//Release the locks held by the transaction
		releaseLocks(transaction);
		
		// If we have to maintain a transaction log, write the transaction log here
		
		// Return true if the commit was successful 
		return commitstatus;
	}
	
	/**
	 * Release the locks acquired by a transactions
	 * Invoked during commit and aborts (due to timeout)
	 * @param transaction
	 */
	public void releaseLocks(Transaction transaction) {
		
		//Release Write locks
		for(DataItem writeItem: transaction.getWriteSet()){
		  this.writeLocks.remove(writeItem.getPrimaryKey());
		  //TODO: Check correctness
	      LOG.debug("Releasing write lock on key:"+ writeItem.getPrimaryKey());
		  this.writeLocksReleased.add(writeItem.getPrimaryKey());
		}
				
		// Release Read Locks
		for(DataItem readItem: transaction.getReadSet()){
		  List<Long> existingLocks = this.readLocks.get(readItem.getPrimaryKey());
		  //TODO: Make sure code will still be thread safe 
		  Boolean removed = false;
		  // Null check is for sanity check. But the lock should exist, since we only release existing locks
		  if(existingLocks!=null && existingLocks.size() == 1 && 
			existingLocks.get(0).equals(transaction.getTransactionId())){
			// Thread Safe as we are using concurrent hash map
			removed = this.readLocks.remove(readItem.getPrimaryKey(), existingLocks);
		  } 
		  if(!removed) {
			// This indicates there is more than 1 read lock
		    this.readLocks.get(readItem.getPrimaryKey()).remove(
			  transaction.getTransactionId());
		  }
		  LOG.debug("Releasing read lock on key:"+ readItem.getPrimaryKey());
		  this.readLocksReleased.add(readItem.getPrimaryKey());
		}		
	}
	/**
	 * 
	 * @param primaryKey
	 * @return
	 */
	public Boolean releaseWaitingLocksFromQueue(String primaryKey) {
		//Release Write Locks
		if(this.writeLockQueue.containsKey(primaryKey)){
	      WaitLockRequest lockRequestRelased = this.writeLockQueue.get(primaryKey).poll();
	      if(lockRequestRelased!=null){
	    	  Thread lockRequestThread = lockRequestRelased.getLockRequestThread();
		      Long transactionId = lockRequestRelased.getTransactionId();
		      LOG.debug("Checking if transactionId:"+transactionId+" can be granted the write lock " +
		    		  "to key:"+primaryKey);
		      //Added: a synchronization block for correctness
		      if(!this.writeLocks.containsKey(primaryKey) && (!this.readLocks.containsKey(primaryKey) || (
						this.readLocks.get(primaryKey).size() == 1 && 
						this.readLocks.get(primaryKey).get(0) == transactionId))) {
		    	  // TODO: Look at optimizing this - May be synchronizing taking the primary key into account
		    	  synchronized (this) {
		    		  // TODO: The check for the state of the thread is just for extra safety
		    		  if(!this.writeLocks.containsKey(primaryKey) && (!this.readLocks.containsKey(primaryKey) || (
								this.readLocks.get(primaryKey).size() == 1 && 
								this.readLocks.get(primaryKey).get(0) == transactionId))
								&&  lockRequestThread.getState().equals(Thread.State.TIMED_WAITING)) {
		    			  
		    			  this.writeLocks.put(primaryKey, transactionId);
				    	  lockRequestThread.interrupt();	  
				    	  LOG.debug("Granted write lock on key "+primaryKey + "to transactionId:"+transactionId);
				    	  return true;  
		    		  }	
				} 	  
		      }
	      }
	     
		}
		
		//Release Read locks
		if(this.readLockQueue.containsKey(primaryKey)) {
		  WaitLockRequest lockRequestRelased = this.readLockQueue.get(primaryKey).poll();
		  if(lockRequestRelased!=null){
			  Thread lockRequestThread = lockRequestRelased.getLockRequestThread();
			  Long transactionId = lockRequestRelased.getTransactionId();
			  LOG.debug("Checking if transactionId:"+transactionId+" can be granted the read lock " +
			  		"to key:"+primaryKey);
			  //TODO: should we a extra synchronization block here
			  if(!this.writeLocks.containsKey(primaryKey)) {
				  //TODO: Look at optimizing this
				  synchronized (this) {
					 
					  if(!this.writeLocks.containsKey(primaryKey) &&  lockRequestThread.getState().equals(Thread.State.TIMED_WAITING)) {
					    this.readLocks.putIfAbsent(primaryKey, new ArrayList<Long>());
						this.readLocks.get(primaryKey).add(transactionId);
						lockRequestThread.interrupt();
						LOG.debug("Granted read lock on key "+primaryKey + " to transactionId:"+transactionId);
						return true;  
					  }
				}
				
			  }
		  }
		}
		return false;
	}

	@Override
	public void run() {
		// TODO Background process that 
		// looks at the lock queue and processes it
		
	  // Once a lock is released and transactions are waiting on that lock
	  // then transactions are given access based on the type of lock requested 
		
		//Also releases the lock and aborts the transaction if the locks timeout
		
		while(true) {
			String readKey = this.readLocksReleased.poll();
			while(readKey!=null){
		      releaseWaitingLocksFromQueue(readKey);
		      readKey = this.readLocksReleased.poll();
			}
					
			String writeKey = this.writeLocksReleased.poll();
			while(writeKey!=null){
		      releaseWaitingLocksFromQueue(writeKey);
		      writeKey = this.writeLocksReleased.poll();
			}
					
			try {
				Thread.sleep(BACKGROUND_WAIT);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	class WaitLockRequest {
		Long transactionId;
		Thread lockRequestThread;
		
		WaitLockRequest(Long transactionId, Thread lockRequestThread) {
			this.transactionId = transactionId;
			this.lockRequestThread = lockRequestThread;
		}
		
		public Long getTransactionId () {
			return this.transactionId;
		}
		
		public Thread getLockRequestThread () {
			return this.lockRequestThread;
		}
	}

}
