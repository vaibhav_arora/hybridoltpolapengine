package transactionalClient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import messaging.HybridEngine.AnalyticalOperationRequest;
import messaging.HybridEngine.AnalyticalOperationResponse;
import messaging.HybridEngine.HybridStoreService.BlockingInterface;
import messaging.HybridEngine.TransactionalCommitRequest;
import messaging.HybridEngine.TransactionalOperationRequest;
import messaging.HybridEngine.TransactionalOperationResponse;

import org.apache.log4j.Logger;

import partitioning.PartitioningFunction;
import rpc.RPCClient;
import utilities.Constants;
import utilities.Operation;
import utilities.OperationResult;
import utilities.Transaction;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.googlecode.protobuf.socketrpc.SocketRpcController;

/**
 * Receives request from the client and routes the transactional 
 * and analytical requests according
 * @author vaibhav
 *
 */
public class TransactionalClient {

  /**
   * Routes the client request to either the transactional
   * engine or analytical engine
   * @param clientRequest
   */
	
  RPCClient[] transactionalEngineRPCClient;
  RPCClient[] analyticalEngineRPCClient;	
  private final static Logger LOG = Logger.getLogger(TransactionalClient.class);
  PartitioningFunction rowPartitioningFunction;
  PartitioningFunction columnPartitioningFunction;
  
  Boolean partitioningEnabled;
  Boolean distributedTransactionsEnabled;
  Map<Long, Transaction> transactionMap;
  /**
   * Maps each transactionId to a partition
   * If it is a distributed transaction, transactionId is mapped to it's 2PC coordinator
   * Otherwise, transaction is mapped to the partition it is invoked on
   */
  Map<Long, Integer> transactionToPartitionMapping;

  public TransactionalClient(RPCClient transactionalEngineRPCClient, 
		  RPCClient analyticalEngineRPCClient) {
	  this.transactionalEngineRPCClient[0] = transactionalEngineRPCClient;
	  this.analyticalEngineRPCClient[0] = analyticalEngineRPCClient;
	  this.partitioningEnabled = false;
	  this.distributedTransactionsEnabled = false;
	  this.transactionMap = new  ConcurrentHashMap<Long, Transaction>();
  }
  
  /**
   * For invoking a partitioning enabled transactional client/Execution Engine
   * @param transactionalEngineRPCClient
   * @param analyticalEngineRPCClient
   * @param rowPartitioningFunction
   * @param columnPartitioningFunction
   */
  public TransactionalClient(Integer numRowPartitions, RPCClient[] transactionalEngineRPCClient, 
      Integer numColumnPartitions, RPCClient[] analyticalEngineRPCClient, PartitioningFunction rowPartitioningFunction, 
      PartitioningFunction columnPartitioningFunction) {
	for(int i=0; i < numRowPartitions; i++){
	  // Partitions are id from 0 to numRowPartitions
	  this.transactionalEngineRPCClient[i] = transactionalEngineRPCClient[i];
	}
	for(int i=0; i < numColumnPartitions; i++){
	  // Column partitions are id'ed from 0 to numColumnPartitions
		this.analyticalEngineRPCClient[i] = analyticalEngineRPCClient[i];
	}
   
    this.analyticalEngineRPCClient = analyticalEngineRPCClient;
    this.rowPartitioningFunction = rowPartitioningFunction;
    this.columnPartitioningFunction = columnPartitioningFunction;
    this.partitioningEnabled = true;
    this.distributedTransactionsEnabled = false;
    this.transactionMap = new  HashMap<Long, Transaction>();
  }
  
  public OperationResult routeClientOperation(String clientRequest) throws ServiceException{
	String[] clientRequestParts = clientRequest.split(Constants.QUERY_DELIMETER);
	OperationResult operationalResult = new OperationResult();
	
    if(clientRequestParts[0].startsWith(Constants.ANALYTICAL)){
    	// Analytical Request
    	operationalResult = routeToAnalyticalEngine(clientRequest);
    } else {
    	// Transactional Request
    	operationalResult = routeToTransactionEngine(clientRequest);
    }
    
    return operationalResult;
  }
  
  public OperationResult routeToTransactionEngine(String transactionalRequest) throws ServiceException {
    // Parse the type of transactional request and route it accordingly
	String transactionalRequestParts[] = transactionalRequest.split(Constants.QUERY_DELIMETER);
	
	OperationResult transactionalResult = new OperationResult();
	
	// Invoke the service and the controller for the call
	BlockingInterface service = null ;
	if(!partitioningEnabled){
	  service = transactionalEngineRPCClient[0].getClientBlockingInterface();
	} else {
	  // TODO : Data is partitioned over servers
	} 
	
	RpcController controller = new SocketRpcController();
	
	long transactionId = Long.parseLong(transactionalRequestParts[Constants.
	                                                              TRANSACTION_ID_INDEX]);
	
	TransactionalOperationResponse transactionalOperationResponse = null;
	
	if(transactionalRequestParts[Constants.REQUEST_INDEX].
			startsWith(Constants.BEGIN)){
		/**
		TransactionalBeginRequest transactionalBeginRequest = TransactionalBeginRequest.newBuilder().
				setTransactionId(transactionId).build();
		transactionalOperationResponse  = service.
				beginTransaction(controller, transactionalBeginRequest);
		**/
	  
	  // Handling the begin at Execution Engine itself
	  
	  handleTransactionalBegin(transactionId);
	 
    
	} else if(transactionalRequestParts[Constants.REQUEST_INDEX].
			startsWith(Constants.COMMIT)){
		
	  checkIfTransactionIdExists(transactionId);
	  Transaction currentTransaction = transactionMap.get(transactionId);
	  
	  if(partitioningEnabled) {
	    if(transactionMap.get(transactionId).isTransactionDistributed()) {
	      // Distributed Transaction Processing
	      // TODO: Find out the 2PC coordinator for the transaction and 
	      // If it is a commit request, first send the "Prepare" to the 2PC coordinator 
	      // and the other cohorts
	      // We also have to send the list of other transactions involved, because 
	      // that has to be added to the batch, and is utilized in ensuring consistent analytical 
	      // transactions on the column store
	      for(Integer partitionId: currentTransaction.getPartitionsInvolved()){
	        // Send a prepare message to the partitionId
	      }
	       
	      // If the response of all the participants is commit, send the 
	      // Commit to all the participant partitions
	      // If any of the responses is Abort, abort the transaction and send an 
	      // abort message to all the partitions
	      
	    } else {
	      // Get the partition transaction is mapped to
	      if(!transactionToPartitionMapping.containsKey(transactionId)) {
	        LOG.error("Transaction with Id:"+transactionId+" is not mapped to any partition.");
	      }
	      int partitionId = transactionToPartitionMapping.get(transactionId);
	      // Get the service invoker for the partition Id
	      service = this.transactionalEngineRPCClient[partitionId].getClientBlockingInterface();
	    }
	    
	    Boolean commitStatus = sendTransactionalCommitRequest(transactionId, controller, service);   
		} else {
		  Boolean commitStatus = sendTransactionalCommitRequest(transactionId, controller, service);
		}
	  
		
	} else if((transactionalRequestParts[Constants.REQUEST_INDEX].
			startsWith(Constants.READ)) || (transactionalRequestParts[Constants.REQUEST_INDEX].
					startsWith(Constants.WRITE)) ) {
	  
	  checkIfTransactionIdExists(transactionId);
	  Transaction currentTransaction = transactionMap.get(transactionId);
	  
		String operationRequest= "";
		for(int i = Constants.TRANSACTION_ID_INDEX + 1;i < transactionalRequestParts.length;i++){
		  operationRequest += transactionalRequestParts[i] + " ";
		}
		if(operationRequest.length() == 0){
			throw new RuntimeException("Error in the Query. Empty Exception string");
		}
		
		Operation operation = new Operation(operationRequest);
		if(partitioningEnabled) {
		  //  TODO: Find which partition the query/operation belongs to and forward the request there
      // Assign a 2PC coordinator for the transaction 
		  // Parse the primary key for this
		  Integer primaryKey = parsePrimaryKey(operation.getPrimaryKey());
		  Integer partitionId = rowPartitioningFunction.getPartitionId(primaryKey);
		  // Get the service invoker for the partition Id
		  if(!transactionToPartitionMapping.containsKey(transactionId)){
		    transactionToPartitionMapping.put(transactionId, partitionId);
		  } else {
		    long coordinatorPartition = transactionToPartitionMapping.get(transactionId);
		    if(coordinatorPartition != partitionId){
		      // This is a distributed transaction
		      currentTransaction.setTransactionDistributed();
		    }
		    currentTransaction.addPartition(partitionId);
		  }
		  service = transactionalEngineRPCClient[partitionId].getClientBlockingInterface();
    }
		
		TransactionalOperationRequest transactionalOperationRequest = TransactionalOperationRequest.newBuilder().
				setTransactionId(transactionId).
				setOperation(operationRequest).build();
		 transactionalOperationResponse = service.transactionalOperation
				(controller, transactionalOperationRequest);
		 
	} else {
		// For Select, Insert and Update statements
		// If there is a invalid statement, then parser will throw the exception
	  
	  checkIfTransactionIdExists(transactionId);
	  Transaction currentTransaction = transactionMap.get(transactionId);
	  
		String operationRequest= "";
		for(int i = Constants.TRANSACTION_ID_INDEX + 1;i < transactionalRequestParts.length;i++){
		  operationRequest += transactionalRequestParts[i] + " ";
		}
		if(operationRequest.length() == 0){
			throw new RuntimeException("Error in the Query. Empty Exception string");
		}
		
		Operation operation = new Operation(operationRequest);
    if(partitioningEnabled) {
      //  TODO: Find which partition the query/operation belongs to and forward the request there
      // Assign a 2PC coordinator for the transaction 
      // Parse the primary key for this
      Integer primaryKey = parsePrimaryKey(operation.getPrimaryKey());
      Integer partitionId = rowPartitioningFunction.getPartitionId(primaryKey);
      // Get the service invoker for the partition Id
      if(!transactionToPartitionMapping.containsKey(transactionId)){
        transactionToPartitionMapping.put(transactionId, partitionId);
      } else {
        long coordinatorPartition = transactionToPartitionMapping.get(transactionId);
        if(coordinatorPartition != partitionId){
          // This is a distributed transaction
          currentTransaction.setTransactionDistributed();
        }
        currentTransaction.addPartition(partitionId);
      }
      service = transactionalEngineRPCClient[partitionId].getClientBlockingInterface();
    } 
    
		TransactionalOperationRequest transactionalOperationRequest = TransactionalOperationRequest.newBuilder().
				setTransactionId(transactionId).
				setOperation(operationRequest).build();
		 transactionalOperationResponse = service.transactionalOperation
				(controller, transactionalOperationRequest);
	}
	
	if(transactionalOperationResponse!=null){
		transactionalResult.setOperationStatus(transactionalOperationResponse.
				getOperationSuccessful());
	} else {
		LOG.error("Operation Response is null");
	}
	
	return transactionalResult;
  }
  
  public OperationResult routeToAnalyticalEngine(String analyticalRequest) throws ServiceException {
    // Route the analytical request to the analytical engine
	// Parse the type of transactional request and route it accordingly
	String analyticalRequestParts[] = analyticalRequest.split(Constants.QUERY_DELIMETER);
	  
	OperationResult analyticalResult = new OperationResult();
	
	long analyticalId = Long.parseLong(analyticalRequestParts[Constants.
		                                                              ANALYTICAL_ID_INDEX]);
	
	// Invoke the service and the controller for the call
	// TODO: Currently, it assumes there is only a single column partition
	BlockingInterface service = analyticalEngineRPCClient[0].getClientBlockingInterface();
	RpcController controller = new SocketRpcController();
	
	String operation= "";
	for(int i = Constants.ANALYTICAL_ID_INDEX + 1;i < analyticalRequestParts.length;i++){
		operation += analyticalRequestParts[i];
	}
	if(operation.length() == 0){
		throw new RuntimeException("Error in the Query. Empty Exception string");
	}
	
	AnalyticalOperationRequest analyticalOperationRequest = AnalyticalOperationRequest.
			newBuilder().setOperationId(analyticalId).
			setQuery(operation).build();
		
	AnalyticalOperationResponse analyticalOperationResponse = service.analyticalOperation(controller, 
			analyticalOperationRequest);
	
	return analyticalResult;
  }
  
  public void handleTransactionalBegin(long transactionId) {
    
    if(this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id already exists");
      throw new RuntimeException("Transaction Id already exists");
    }
    LOG.debug("Invoking a transaction with Id: "+transactionId);
    Transaction transaction = new Transaction(transactionId);
    this.transactionMap.put(transactionId, transaction);
    
  }
  
  public void checkIfTransactionIdExists(long transactionId) {
    if(!this.transactionMap.containsKey(transactionId)){
      LOG.error("Transaction with this Id does not exist:"+transactionId);
      throw new RuntimeException("Transaction Id does not exist");
    }
  }
  
  public Integer parsePrimaryKey(String primaryKey) {
    //TODO: Parse the primary key to an integral key.
    // Current implementation only supports integer partition keys
    // TODO: Add support for a more generic case
    return null;
  }
  
  public Boolean sendTransactionalCommitRequest(long transactionId, RpcController controller, 
      BlockingInterface service) throws ServiceException{
    
    TransactionalCommitRequest transactionalCommitRequest = TransactionalCommitRequest.newBuilder().
        setTransactionId(transactionId).build();
    TransactionalOperationResponse transactionalOperationResponse = service.
        commitTransaction(controller, transactionalCommitRequest);
    
    return transactionalOperationResponse.getOperationSuccessful(); 
  }
  
}
