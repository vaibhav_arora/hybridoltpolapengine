package transactionalClient;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import messaging.HybridEngine.ApplicationClientRequest;
import messaging.HybridEngine.ApplicationClientResponse;
import messaging.HybridEngine.HybridStoreService;
import messaging.HybridEngine.HybridStoreService.BlockingInterface;
import messagingLayer.MessagingCoordinator;

import org.apache.log4j.Logger;

import rpc.RPCClient;
import rpc.RPCServer;
import transactionalEngine.LockBasedConcurrencyScheme;
import transactionalEngine.TransactionBatchProcessor;
import transactionalEngine.TransactionalManager;
import utilities.Constants;
import analyticalEngine.AnalyticalEngine;
import analyticalEngine.ColumnStoreDeltaManager;

import com.google.protobuf.RpcController;
import com.googlecode.protobuf.socketrpc.SocketRpcController;

import databaseDriver.MonetDBDriver;
import databaseDriver.MonetDbDriverTransactional;
import databaseDriver.MySQLDriver;

public class ApplicationClient {
	
	MessagingCoordinator messagingCoordinator;

	TransactionalClient transactionalClient;
	TransactionalManager transactionalManager;
	AnalyticalEngine analyticalEngine;

	LockBasedConcurrencyScheme lockBasedConcurrencyScheme;
	TransactionBatchProcessor transactionBatchProcessor;

	MySQLDriver mySQLDriver;
	MonetDBDriver monetDBDriver;
	MonetDbDriverTransactional monetDbDriverTransactional;

	String dbUrl;
	String user;
	String password;
	
	String analyticalDBUrl;
  String analyticalDBuser;
  String analyticalDBpassword;

  RPCClient transactionClientRPCClient;
	RPCClient transactionalEngineRPCClient;
	RPCClient analyticalEngineRPCClient;

	RPCServer transactionalClientRPCServer;
	RPCServer transactionalEngineRPCServer;
	RPCServer analyticalEngineRPCServer;

	String transactionalClientHost;
	String transactionalEngineHost;
	String analyticalEngineHost;

	int analyticalEnginePort;
	int transactionalEnginePort;
	int transactionalClientPort;
	
	Random rnGenerator;
	public BufferedWriter bw;
	int clientsDone;
	long timeFinished;
	long totalCommits;
	
	private final static Logger LOG = Logger.getLogger(ApplicationClient.class);
	
	public ApplicationClient() {
		
		dbUrl = "jdbc:mysql://127.0.0.1:3306/test";

		// currently just connect to the localhost default user with no password
		user = "";
		password = "";
		
		analyticalDBUrl = "jdbc:monetdb://127.0.0.1/test";
		analyticalDBuser = "monetdb";
		analyticalDBpassword = "monetdb";

		// currently hostnames are set to local host
		transactionalClientHost = "127.0.0.1";
		transactionalEngineHost = "127.0.0.1";
		analyticalEngineHost = "127.0.0.1";

		transactionalClientPort = 8081;
		analyticalEnginePort = 8082;
		transactionalEnginePort = 8083;

		transactionClientRPCClient = new RPCClient(transactionalClientHost, 
				transactionalClientPort);
		transactionalEngineRPCClient = new RPCClient(transactionalEngineHost,
				transactionalEnginePort);
		analyticalEngineRPCClient = new RPCClient(analyticalEngineHost,
				analyticalEnginePort);

		// since we are implementing our own scheme auto commit is turned off
		boolean autocommit = false;
		boolean monetDBAutoCommit = true;

		mySQLDriver = new MySQLDriver(dbUrl, user, password, autocommit);
		monetDbDriverTransactional = new MonetDbDriverTransactional
				(analyticalDBUrl, analyticalDBuser, analyticalDBpassword, monetDBAutoCommit);
		// mySQLDriver = Mockito.mock(MySQLDriver.class);
		monetDBDriver = new MonetDBDriver(analyticalDBUrl, analyticalDBuser, 
		    analyticalDBpassword, monetDBAutoCommit);

		lockBasedConcurrencyScheme = new LockBasedConcurrencyScheme(mySQLDriver);

		int partitionID = 1;
		int initBatchID = 1;
		this.transactionBatchProcessor = new TransactionBatchProcessor(
				analyticalEngineRPCClient, initBatchID, partitionID);

		this.transactionalManager = new TransactionalManager(
				lockBasedConcurrencyScheme, transactionBatchProcessor);
		
		int batchIdToIngest = 1;
		int partitionSize = 1;
		ColumnStoreDeltaManager columnStoreDelta = new ColumnStoreDeltaManager
		    (batchIdToIngest, partitionSize, monetDBDriver);
		
		analyticalEngine = new AnalyticalEngine(columnStoreDelta, monetDBDriver);

		transactionalClient = new TransactionalClient(
				transactionalEngineRPCClient, analyticalEngineRPCClient);

		messagingCoordinator = new MessagingCoordinator(transactionalClient,
				analyticalEngine, transactionalManager);

		HybridStoreService transactionalClientService = messagingCoordinator
				.initHybridStoreService();
		HybridStoreService transactionalEngineService = messagingCoordinator
				.initHybridStoreService();
		HybridStoreService analyticalEngineService = messagingCoordinator
				.initHybridStoreService();

		transactionalClientRPCServer = new RPCServer(
				transactionalClientService, transactionalClientPort);
		transactionalEngineRPCServer = new RPCServer(
				transactionalEngineService, transactionalEnginePort);
		analyticalEngineRPCServer = new RPCServer(analyticalEngineService,
				analyticalEnginePort);

		// starting all the three RPC Servers
		transactionalClientRPCServer.start();
		transactionalEngineRPCServer.start();
		analyticalEngineRPCServer.start();
		

		// Experimental additions
		
		rnGenerator = new Random();
		clientsDone = 0;
		totalCommits = 0;
		
		String fileName = "resources/testRuns/"+"output.txt" ;
		bw = null;
		try {
	      File outFile = new File(fileName);
		  bw = new BufferedWriter(new FileWriter(outFile));
		} catch (FileNotFoundException e) {
		  e.printStackTrace();
		} catch (IOException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		}
	}
	
	public class ClientRun implements Runnable{
		int clientId;
		int numTransactions;
		int numOperations;
		int numReads;
		int numItems;
		
		public ClientRun(Integer clientId, int numTransactions, int numItems, 
				int numOperations, int numReads) {
			this.clientId = clientId;
			this.numTransactions = numTransactions;
			this.numOperations = numOperations;
			this.numReads = numReads;
			this.numItems = numItems;
		}
		@Override
		public void run() {
			// Process the test file and send requests to the locking manager after parsing them
			try{
			  // Creating 2 transactions for the simple test to see that the requests 
			  // can't get conflicting requests	
			  
			  // Each client 	
			  int numCommits = 0;
				
			  BlockingInterface service = transactionClientRPCClient.getClientBlockingInterface();
			  RpcController controller = new SocketRpcController();
				
			  for(int i=0; i < numTransactions; i++){
				// For each transaction  numOperations are generated
				
				long transactionId = numTransactions * clientId + i;
				//Begin Request
				//transactionalManager.beginTransaction(transactionId);
				String beginRequest = "transactional "+ transactionId+ 
				    Constants.QUERY_DELIMETER + Constants.BEGIN;
				ApplicationClientRequest applicationClientRequest = ApplicationClientRequest.newBuilder().
						setDataOperation(beginRequest).build();
				
				service.processDataOperation(controller, applicationClientRequest);
				
				// First send numReads read operations
			    for(int j=0; j < numReads; j++) {
			    	String readQueryPrefix = Constants.READ + Constants.QUERY_DELIMETER;
			    	String primaryKeyPrefix = "test#TestTable#table_key#" ; 
			    	
			    	int itemToRead = rnGenerator.nextInt(numItems)+1;
			    	String keyToRead = primaryKeyPrefix + itemToRead;
			    	
			    	String readQuery = readQueryPrefix + keyToRead;
			    	
			    	//Operation readOperation = new Operation(readQuery);
			        //transactionalManager.processReadRequest(transactionId, readOperation);	
			        //  Sending the request to the transactional client server instead
			        String readRequest = "transactional "+ transactionId + Constants.QUERY_DELIMETER + readQuery;
			        
					ApplicationClientRequest applicationClientReadRequest = ApplicationClientRequest.newBuilder().
							setDataOperation(readRequest).build();
					
					ApplicationClientResponse applicationClientReadResponse = 
					    service.processDataOperation(controller, 
							applicationClientReadRequest);
			    }
			    // Then send write operations 
			    int numWrites = numOperations - numReads;
                for(int j=0; j < numWrites; j++) {
                  String writeQueryPrefix = Constants.WRITE + Constants.QUERY_DELIMETER;
			      String primaryKeyPrefix = "test#TestTable#table_key#" ; 
			    	
			      int itemToWrite = rnGenerator.nextInt(numItems)+1;
			      String keyToWrite = primaryKeyPrefix + itemToWrite;
			      String valueToWrite = "Value" + rnGenerator.nextInt(numItems);
			    	
			      String writeQuery = writeQueryPrefix + keyToWrite + Constants.QUERY_FIELD_DELIMETER +
			    		  "value" + Constants.QUERY_FIELD_DELIMETER + "'" + valueToWrite + "'";
			    	
			      //Operation writeOperation = new Operation(writeQuery);
			      //transactionalManager.processWriteRequest(transactionId, writeOperation);
			      // Sending the request to the transactional client server instead
			      String writeRequest = "transactional "+ transactionId+ Constants.QUERY_DELIMETER + writeQuery;
				  ApplicationClientRequest applicationClientWriteRequest = ApplicationClientRequest.newBuilder().
							setDataOperation(writeRequest).build();
					
					ApplicationClientResponse applicationClientWriteResponse = service.processDataOperation(controller, 
							applicationClientWriteRequest);
			    }
                
                //Then send a commit operation
                //Boolean didCommit = transactionalManager.processCommitRequest(transactionId);
                // Sending the request to RPC Server now
                String commitRequest = "transactional "+ transactionId+ Constants.QUERY_DELIMETER + 
                		Constants.COMMIT;
				ApplicationClientRequest applicationClientCommitRequest = ApplicationClientRequest.newBuilder().
						setDataOperation(commitRequest).build();
				
				ApplicationClientResponse applicationClientCommitResponse = service.processDataOperation(controller, 
						applicationClientCommitRequest);
				
				Boolean didCommit = applicationClientCommitResponse.getOperationSuccessful();
				
                if(didCommit) {
                	numCommits++;
                }
                
			  }		
			  	  
			  
			  // Write commits to file
			  writeCommitsToFile(clientId, numCommits);
			  long timeFinished = System.currentTimeMillis();
			  registerEndOfRun(clientId, numCommits, timeFinished);
			} catch(Exception e) {
				LOG.error("Caught exception",e);
			}		
		}
	}
	
	public void writeCommitsToFile(int clientId, int numCommits) {
		try {
			bw.write("Client Id:"+clientId+" "+numCommits+"\n");
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
	public void registerEndOfRun(int clientId, int numCommits, long timeFinished) {
		clientsDone++;
		totalCommits += numCommits; 
		this.timeFinished = Math.max(timeFinished, this.timeFinished);
	}
	
	public int getClientsDone() {
		return this.clientsDone;
	}
	
	public long getTimeFinished() {
		return this.timeFinished;
	}
	
	public long getTotalCommits() {
		return this.totalCommits;
	}
	
	public static void main(String[] args){
		ApplicationClient applicationClient = new ApplicationClient();
		
		
		int numclients = 10;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(numclients+10);
		int numTransactions = 100;
		int numOperations = 5;
		int numReads = 3;
		int numItems = 1000;
		
		taskExecutor.execute(applicationClient.lockBasedConcurrencyScheme);
		//taskExecutor.execute(applicationClient.transactionBatchProcessor);
    try {
      Thread.sleep(100L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
		long startTime = System.currentTimeMillis();
		
		for(int i= 0 ; i < numclients; i++ ){
			taskExecutor.execute(applicationClient.new ClientRun(i, numTransactions
					, numItems, numOperations, numReads));	
		}
		
		while(true){
			if(applicationClient.clientsDone == numclients){
				try {
					applicationClient.bw.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				System.out.println("All clients are done");
				long experimentDuration = applicationClient.timeFinished - 
						startTime;
				
				System.out.println("Experiment duration: "+experimentDuration+"ms Total Commits: "
						+ applicationClient.getTotalCommits());
				break;
			}
			try {
				Thread.sleep(100L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Shutdown the servers
		applicationClient.transactionalClientRPCServer.stop();
		applicationClient.transactionalEngineRPCServer.stop();
		applicationClient.analyticalEngineRPCServer.stop();
		
	}

}
