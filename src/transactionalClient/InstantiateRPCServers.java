package transactionalClient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import messaging.HybridEngine.HybridStoreService;
import messagingLayer.MessagingCoordinator;
import rpc.RPCClient;
import rpc.RPCServer;
import transactionalEngine.LockBasedConcurrencyScheme;
import transactionalEngine.TransactionBatchProcessor;
import transactionalEngine.TransactionalManager;
import analyticalEngine.AnalyticalEngine;
import analyticalEngine.ColumnStoreDeltaManager;
import databaseDriver.MonetDBDriver;
import databaseDriver.MonetDbDriverTransactional;
import databaseDriver.MySQLDriver;

public class InstantiateRPCServers {
  
  MessagingCoordinator messagingCoordinator;

  TransactionalClient transactionalClient;
  TransactionalManager transactionalManager;
  AnalyticalEngine analyticalEngine;

  LockBasedConcurrencyScheme lockBasedConcurrencyScheme;
  TransactionBatchProcessor transactionBatchProcessor;

  MySQLDriver mySQLDriver;
  MonetDBDriver monetDBDriver;
  MonetDbDriverTransactional monetDbDriverTransactional;

  String dbUrl;
  String user;
  String password;
  
  String analyticalDBUrl;
  String analyticalDBuser;
  String analyticalDBpassword;

  RPCClient transactionClientRPCClient;
  RPCClient transactionalEngineRPCClient;
  RPCClient analyticalEngineRPCClient;

  RPCServer transactionalClientRPCServer;
  RPCServer transactionalEngineRPCServer;
  RPCServer analyticalEngineRPCServer;

  String transactionalClientHost;
  String transactionalEngineHost;
  String analyticalEngineHost;

  int analyticalEnginePort;
  int transactionalEnginePort;
  int transactionalClientPort;
  
  public InstantiateRPCServers() {
    dbUrl = "jdbc:mysql://127.0.0.1:3306/test";

    // currently just connect to the localhost default user with no password
    user = "";
    password = "";
    
    analyticalDBUrl = "jdbc:monetdb://127.0.0.1/test";
    analyticalDBuser = "monetdb";
    analyticalDBpassword = "monetdb";

    // currently hostnames are set to local host
    transactionalClientHost = "127.0.0.1";
    transactionalEngineHost = "127.0.0.1";
    analyticalEngineHost = "127.0.0.1";

    transactionalClientPort = 8081;
    analyticalEnginePort = 8082;
    transactionalEnginePort = 8083;

    transactionClientRPCClient = new RPCClient(transactionalClientHost, 
        transactionalClientPort);
    transactionalEngineRPCClient = new RPCClient(transactionalEngineHost,
        transactionalEnginePort);
    analyticalEngineRPCClient = new RPCClient(analyticalEngineHost,
        analyticalEnginePort);

    // since we are implementing our own locking scheme auto commit is turned on. Verify ?
    boolean autocommit = true;
    boolean monetDBAutoCommit = true;

    mySQLDriver = new MySQLDriver(dbUrl, user, password, autocommit);
    monetDbDriverTransactional = new MonetDbDriverTransactional
        (analyticalDBUrl, analyticalDBuser, analyticalDBpassword, monetDBAutoCommit);
    // mySQLDriver = Mockito.mock(MySQLDriver.class);
    monetDBDriver = new MonetDBDriver(analyticalDBUrl, analyticalDBuser, 
        analyticalDBpassword, monetDBAutoCommit);

    lockBasedConcurrencyScheme = new LockBasedConcurrencyScheme(mySQLDriver);

    int partitionID = 1;
    int initBatchID = 1;
    this.transactionBatchProcessor = new TransactionBatchProcessor(
        analyticalEngineRPCClient, initBatchID, partitionID);

    this.transactionalManager = new TransactionalManager(
        lockBasedConcurrencyScheme, transactionBatchProcessor);
    
    int batchIdToIngest = 1;
    int partitionSize = 1;
    ColumnStoreDeltaManager columnStoreDelta = new ColumnStoreDeltaManager
        (batchIdToIngest, partitionSize, monetDBDriver);
    
    analyticalEngine = new AnalyticalEngine(columnStoreDelta, monetDBDriver);

    transactionalClient = new TransactionalClient(
        transactionalEngineRPCClient, analyticalEngineRPCClient);

    messagingCoordinator = new MessagingCoordinator(transactionalClient,
        analyticalEngine, transactionalManager);

    HybridStoreService transactionalClientService = messagingCoordinator
        .initHybridStoreService();
    HybridStoreService transactionalEngineService = messagingCoordinator
        .initHybridStoreService();
    HybridStoreService analyticalEngineService = messagingCoordinator
        .initHybridStoreService();

    transactionalClientRPCServer = new RPCServer(
        transactionalClientService, transactionalClientPort);
    transactionalEngineRPCServer = new RPCServer(
        transactionalEngineService, transactionalEnginePort);
    analyticalEngineRPCServer = new RPCServer(analyticalEngineService,
        analyticalEnginePort);

    // starting all the three RPC Servers
    transactionalClientRPCServer.start();
    transactionalEngineRPCServer.start();
    analyticalEngineRPCServer.start();
  }
  
  public static void main(String[] args){
    InstantiateRPCServers instantiate = new InstantiateRPCServers();
    ExecutorService taskExecutor = Executors.newFixedThreadPool(10);
    taskExecutor.execute(instantiate.lockBasedConcurrencyScheme);
    while(true){
      try {
        Thread.sleep(100000L);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    
  }

}
