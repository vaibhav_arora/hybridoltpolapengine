package transactionalClient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * For loading data into MySQL for experiments
 * @author vaibhav
 *
 */
public class MySQLLoader {
  
  private final static Logger LOG = Logger.getLogger(MySQLLoader.class);
  private boolean autoCommit;
  private String dbUrl;
  private String user; 
  private String password;
  
  public MySQLLoader() {
    dbUrl = "jdbc:mysql://127.0.0.1:3306/test";
    user = "";
    password = "";
    autoCommit = false;
  }
  
  /**
   * Assumes a schema with (key, value) - (int, String)
   * @param tableName
   * @param numItems
   */
  public void loadItems(String tableName, int numItems) {
    String insertSQLStatement;
    Connection conn;
    
    try {
      conn = DriverManager.getConnection(dbUrl);
      conn.setAutoCommit(autoCommit);
      Statement stmt = conn.createStatement();
      
      for(int i=3; i < numItems; i++){
        insertSQLStatement = "Insert into "+tableName+" values("+i+
            ", 'Value"+i+"')";
        stmt.addBatch(insertSQLStatement);
      }
      
      stmt.executeBatch();
      conn.commit();
    } catch (SQLException e) {
      LOG.error("Exception occured ", e);
    }
  }
  
  public static void main(String[] args){
    MySQLLoader mySQLLoader = new MySQLLoader();
    int numItemsToLoad = 1000;
    String tableName = "TestTable";
    // Load Items into the table
    mySQLLoader.loadItems(tableName, numItemsToLoad);
    
  }

}
