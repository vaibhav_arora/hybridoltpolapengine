package partitioning;

public interface PartitioningFunction {
	
	/**
	 * Given the primaryKey of the item, returns the id of the 
	 * partition it is hosted on 
	 * @param primaryKey
	 * @return
	 */
	public Integer getPartitionId(Integer primaryKey);
	
	/**
	 * Given the id of the partition, this function responds 
	 * @param PartitionId
	 * @return
	 */
	public Integer getServerId(Integer PartitionId);
		
}
