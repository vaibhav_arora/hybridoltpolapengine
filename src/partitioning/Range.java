package partitioning;

public class Range {
	
	Integer startRange;
	Integer endRange;
	
	public Range(Integer startRange, Integer endRange) {
		this.startRange = startRange;
		this.endRange = endRange;
	}
	
	public Boolean isPartOfRange(Integer toCheck) {
		if(toCheck >= startRange && toCheck < endRange){
			return true;
		}
		return false;
	}

}
