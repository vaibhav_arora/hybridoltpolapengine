package partitioning;

import java.util.Map;

public class RangePartitioning implements PartitioningFunction {

	Map<Range, Integer> rangeMap;
	Map<Integer, Integer> partitionToServerMapping;
	
	public RangePartitioning(Map<Range, Integer> rangeMap, 
			Map<Integer, Integer> partitionToServerMapping){
		this.rangeMap = rangeMap;
		this.partitionToServerMapping = partitionToServerMapping;
	}
	
	@Override
	public Integer getPartitionId(Integer primaryKey) {
		for(Range range: rangeMap.keySet()) {
			if(range.isPartOfRange(primaryKey)){
				return rangeMap.get(range);
			}
		}
		// This remains the primary key is not a part of any range
		// in which case a error will be thrown
		return null;
	}

	@Override
	public Integer getServerId(Integer partitionId) {
		if(!partitionToServerMapping.containsKey(partitionId)){
			// If the partitionId is not mapped to a Server
			throw new RuntimeException("Mapping of the partition with Id:"+partitionId+" not " +
					"found");
		}
		return partitionToServerMapping.get(partitionId);
	}

}
