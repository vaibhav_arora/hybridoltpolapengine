package transactionalEngine;

import org.junit.Before;
import org.junit.Test;

import utilities.DataItem;
import utilities.Operation;

import com.google.protobuf.ServiceException;

public class QueryParserTest {
  
  @Before
  public void setUp() {
    
  }
  
  @Test
  public void parseSelectSQLQueryTest() throws ServiceException {
    String getWarehouseSQL = "SELECT C_DISCOUNT, C_LAST, C_CREDIT, W_TAX FROM " +
    		"CUSTOMER, WAREHOUSE WHERE W_ID = 1 AND C_D_ID = 2 AND C_ID = 3";
    Operation operation = new Operation(getWarehouseSQL);
    for(DataItem dataItem: operation.getReadSet()){
      System.out.println(dataItem.primaryKey);
    }
  }
  
  @Test
  public void parseUpdateSQLQueryTest() throws ServiceException {
    String updateStock = "UPDATE STOCK SET QUANTITY = 1, S_YTD = 2, " +
    		"S_ORDER_CNT = 3, S_REMOTE_CNT = 3 WHERE S_I_ID = 3 AND S_W_ID = 4";
    Operation operation = new Operation(updateStock);
    for(DataItem dataItem: operation.getWriteSet()){
      System.out.println(dataItem.primaryKey);
    }
  }

}
