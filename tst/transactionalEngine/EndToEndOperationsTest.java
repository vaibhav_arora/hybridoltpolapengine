package transactionalEngine;

import junit.framework.Assert;
import messaging.HybridEngine.HybridStoreService;
import messagingLayer.MessagingCoordinator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import rpc.RPCClient;
import rpc.RPCServer;
import transactionalClient.TransactionalClient;
import utilities.Constants;
import utilities.TransactionBatch;
import analyticalEngine.AnalyticalDatabase;
import analyticalEngine.AnalyticalEngine;
import analyticalEngine.ColumnStoreDeltaManager;

import com.google.protobuf.ServiceException;

import databaseDriver.MonetDBDriver;
import databaseDriver.MySQLDriver;

public class EndToEndOperationsTest {

	MessagingCoordinator messagingCoordinator;

	TransactionalClient transactionalClient;
	TransactionalManager transactionalManager;
	AnalyticalEngine analyticalEngine;

	LockBasedConcurrencyScheme lockBasedConcurrencyScheme;
	TransactionBatchProcessor transactionBatchProcessor;

	MySQLDriver mySQLDriver;
	MonetDBDriver monetDBDriver;

	String dbUrl;
	String user;
	String password;
	
	String analyticalDBUrl;
    String analyticalDBuser;
    String analyticalDBpassword;

    RPCClient transactionClientRPCClient;
	RPCClient transactionalEngineRPCClient;
	RPCClient analyticalEngineRPCClient;

	RPCServer transactionalClientRPCServer;
	RPCServer transactionalEngineRPCServer;
	RPCServer analyticalEngineRPCServer;

	String transactionalClientHost;
	String transactionalEngineHost;
	String analyticalEngineHost;

	int analyticalEnginePort;
	int transactionalEnginePort;
	int transactionalClientPort;

	@Before
	public void setUp() {
		dbUrl = "jdbc:mysql://127.0.0.1:3306/test";

		// currently just connect to the localhost default user with no password
		user = "";
		password = "";
		
		analyticalDBUrl = "jdbc:monetdb://127.0.0.1/test";
		analyticalDBuser = "monetdb";
		analyticalDBpassword = "monetdb";

		// currently hostnames are set to local host
		transactionalClientHost = "127.0.0.1";
		transactionalEngineHost = "127.0.0.1";
		analyticalEngineHost = "127.0.0.1";

		transactionalClientPort = 8081;
		analyticalEnginePort = 8082;
		transactionalEnginePort = 8083;

		transactionClientRPCClient = new RPCClient(transactionalClientHost, 
				transactionalClientPort);
		transactionalEngineRPCClient = new RPCClient(transactionalEngineHost,
				transactionalEnginePort);
		analyticalEngineRPCClient = new RPCClient(analyticalEngineHost,
				analyticalEnginePort);

		// since we are implementing our own scheme auto commit is turned off
		boolean autocommit = false;
		boolean monetDBAutoCommit = false;

		mySQLDriver = new MySQLDriver(dbUrl, user, password, autocommit);
		// mySQLDriver = Mockito.mock(MySQLDriver.class);
		monetDBDriver = new MonetDBDriver(analyticalDBUrl, analyticalDBuser, 
		    analyticalDBpassword, monetDBAutoCommit);

		lockBasedConcurrencyScheme = new LockBasedConcurrencyScheme(mySQLDriver);

		int partitionID = 1;
		int initBatchID = 1;
		this.transactionBatchProcessor = new TransactionBatchProcessor(
				analyticalEngineRPCClient, initBatchID, partitionID);

		transactionalManager = new TransactionalManager(
				lockBasedConcurrencyScheme, transactionBatchProcessor);
		
		int batchIdToIngest = 1;
		int partitionSize = 1;
		ColumnStoreDeltaManager columnStoreDelta = new ColumnStoreDeltaManager
		    (batchIdToIngest, partitionSize, monetDBDriver);
		
		analyticalEngine = new AnalyticalEngine(columnStoreDelta, monetDBDriver);

		transactionalClient = new TransactionalClient(
				transactionalEngineRPCClient, analyticalEngineRPCClient);

		messagingCoordinator = new MessagingCoordinator(transactionalClient,
				analyticalEngine, transactionalManager);

		HybridStoreService transactionalClientService = messagingCoordinator
				.initHybridStoreService();
		HybridStoreService transactionalEngineService = messagingCoordinator
				.initHybridStoreService();
		HybridStoreService analyticalEngineService = messagingCoordinator
				.initHybridStoreService();

		transactionalClientRPCServer = new RPCServer(
				transactionalClientService, transactionalClientPort);
		transactionalEngineRPCServer = new RPCServer(
				transactionalEngineService, transactionalEnginePort);
		analyticalEngineRPCServer = new RPCServer(analyticalEngineService,
				analyticalEnginePort);

		transactionalClientRPCServer.start();
		// System.out.println("Hi");
		transactionalEngineRPCServer.start();
		analyticalEngineRPCServer.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//@Test
	public void testTransactionalBatch() throws ServiceException {

		String clientBeginRequest = "transactional 1 Begin";
		transactionalClient.routeClientOperation(clientBeginRequest);
		String clientReadRequest = "transactional 1 Read test#TestTable#table_key#1";
		transactionalClient.routeClientOperation(clientReadRequest);
		String clientWriteRequest = "transactional 1 Write test#TestTable#table_key#1#"
				+ "value#" + "'Value4'";

		transactionalClient.routeClientOperation(clientWriteRequest);
		String clientCommitRequest = "transactional 1 Commit";
		transactionalClient.routeClientOperation(clientCommitRequest);

		TransactionBatch transactionBatch = this.transactionBatchProcessor
				.getCurrentTransactionBatch();
		Assert.assertTrue(transactionBatch.getBatchId() == 0);
		Assert.assertTrue(transactionBatch.getTransactionList().size() == 1);

	}

	//@Test
	public void testTransactionalOperation() throws ServiceException {

	    String clientBeginRequest = "transactional 2 Begin";
		transactionalClient.routeClientOperation(clientBeginRequest);
		String clientReadRequest = "transactional 2 Read test#TestTable#table_key#1";
		transactionalClient.routeClientOperation(clientReadRequest);
		String clientWriteRequest = "transactional 2 Write test#TestTable#table_key#1#"
				+ "value#" + "'Value5'";

		transactionalClient.routeClientOperation(clientWriteRequest);
		String clientCommitRequest = "transactional 2 Commit";
		transactionalClient.routeClientOperation(clientCommitRequest);
	}

	@Test
	public void sendTransactionalBatchTest() throws ServiceException {

		// Start the batch processor thread
		// TODO: Note that this test only runs when other tests are commented out
		// Looks to be a Junit issue as many  backgroundThread's seem to start
		// when the thread is invoked from the setup() method in case of multiple tests running.
		Thread backgroundThread = new Thread(transactionBatchProcessor);
		backgroundThread.start();
				
		String clientBeginRequest = "transactional 3 Begin";
		transactionalClient.routeClientOperation(clientBeginRequest);
		String clientReadRequest = "transactional 3 Read test#TestTable#table_key#1";
		transactionalClient.routeClientOperation(clientReadRequest);
		String clientWriteRequest = "transactional 3 Write test#TestTable#table_key#1#"
				+ "value#" + "'ValueEndToEndUnitTest2'";

		transactionalClient.routeClientOperation(clientWriteRequest);
		String clientCommitRequest = "transactional 3 Commit";
		transactionalClient.routeClientOperation(clientCommitRequest);

		try {
			Thread.sleep(Constants.BATCH_SENDING_PERIOD + 2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
