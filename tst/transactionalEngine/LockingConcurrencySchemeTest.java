package transactionalEngine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.mockito.Mockito;

import utilities.Constants;
import utilities.Operation;
import utilities.Transaction;
/**
 * Testing with multiple client threads to test concurrency scheme
 * @author vaibhav
 *
 */
public class LockingConcurrencySchemeTest{
	
	LockBasedConcurrencyScheme lockBasedConcurrencyScheme;
	public static TransactionalDatabase transactionalDatabase;
	
	TransactionBatchProcessor transactionBatchProcessor;
	
	TransactionalManager transactionalManager;
	
	private final static Logger LOG = Logger.getLogger(LockingConcurrencySchemeTest.class);
	
	public LockingConcurrencySchemeTest() {
		transactionalDatabase = Mockito.mock(TransactionalDatabase.class);
		transactionBatchProcessor = Mockito.mock(TransactionBatchProcessor.class);
		lockBasedConcurrencyScheme = new LockBasedConcurrencyScheme(transactionalDatabase); 
		
		transactionalManager = new TransactionalManager(
				lockBasedConcurrencyScheme, transactionBatchProcessor);
	}
	

	public class ClientRun implements Runnable{
		Integer clientId;
		public ClientRun(Integer clientId) {
			this.clientId = clientId;
		}
		@Override
		public void run() {
			// Process the test file and send requests to the locking manager after parsing them
			try{
			  // Creating 2 transactions for the simple test to see that the requests 
			  // can't get conflicting requests	
			  int testCase = 2;
			  String fileName = "tst/resources/lockingTest/client"+this.clientId+"test"+testCase+".txt" ;
			  FileReader inputFile = new FileReader(fileName);
			  BufferedReader bf = new BufferedReader(inputFile);
			  String line;
			  while((line=bf.readLine())!=null) {
	  			LOG.debug("Processing Request:"+line);
	  			String[] queryParts = line.split(Constants.QUERY_DELIMETER);
	  			Long tId = Long.parseLong(queryParts[0]);
	  			String query = "";
	  			if(queryParts[1].startsWith(Constants.BEGIN)){			
	  				transactionalManager.beginTransaction(tId);
	  			} else if(queryParts[1].startsWith(Constants.COMMIT)) {
	  				transactionalManager.processCommitRequest(tId);
	  			} else if(queryParts[1].startsWith(Constants.READ)) {
	  				query = queryParts[1] + Constants.QUERY_DELIMETER + queryParts[2];
	  				Operation readOperation = new Operation(query);
	  				transactionalManager.processReadRequest(tId, readOperation);
	  			} else if(queryParts[1].startsWith(Constants.WRITE)) {
	  				query = queryParts[1] + Constants.QUERY_DELIMETER + queryParts[2];
	  				Operation writeOperation = new Operation(query);
	  				transactionalManager.processWriteRequest(tId, writeOperation);
	  			} else {
	  				LOG.error("Error in the query");
	  			}
	  			 			
		      }
				
			} catch(Exception e) {
				LOG.error("Caught exception",e);
			}		
		}
		
	}
	
	
	public static void main(String[] args){
		LockingConcurrencySchemeTest lockingScheme = new LockingConcurrencySchemeTest();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(5);
		int client1 = 1;
		int client2 = 2;
		taskExecutor.execute(lockingScheme.lockBasedConcurrencyScheme);
		taskExecutor.execute(lockingScheme.new ClientRun(client1));
		taskExecutor.execute(lockingScheme.new ClientRun(client2));		
	}

}
