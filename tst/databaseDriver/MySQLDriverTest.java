package databaseDriver;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import utilities.Operation;
import utilities.Transaction;
import utilities.Utils;

public class MySQLDriverTest {

	MySQLDriver mySQLDriver;
	String dbUrl;
	String user;
	String password;
	private final static Logger LOG = Logger.getLogger(MySQLDriverTest.class);

	@Before
	public void setUp(){
		dbUrl = "jdbc:mysql://127.0.0.1:3306/test";
		user = "";
		password = "";
	}
	
	//@Test
	public void connect() {
		mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
	}
	
	@Test
	public void performRead() {
		mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
		String readQuery = "Select value from TestTable where table_key = 1";
		Operation queryOperation = new Operation(readQuery);
		Long transactionId = 1L;
		Transaction transaction = new Transaction(transactionId);
		mySQLDriver.executeQuery(transactionId, queryOperation);
	}
	
	//@Test
	public void performWrite() {
		mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
		String updateQuery = "Update test.TestTable SET value='Value1diff' Where table_key = 1";
		Operation writeOperation = new Operation(updateQuery);
		Long transactionId = 1L;
		Transaction transaction = new Transaction(transactionId);
		mySQLDriver.processWriteRequest(transactionId, writeOperation);
	}
	
	//@Test
	public void performCommit() {
		mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
		Long transactionId = 1L;
		Transaction transaction = new Transaction(transactionId);
		//since the autocommit is on, only the connection is released
		mySQLDriver.processCommit(transactionId);
	}
	
	@Test
	public void testConvertToSQLSelect() {
	  mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
	  String query = "Read test#TestTable#table_key#1";
	  String sqlQuery = Utils.convertToSQLSelect(query);
	  LOG.info("SQL Select Query:" + sqlQuery);
	}
	
	@Test
    public void testConvertToSQLUpdate() {
    mySQLDriver = new MySQLDriver(dbUrl, user, password, true);
    String update = "Write test#TestTable#table_key#1#value#3";
    String sqlUpdateStmt = Utils.convertToSQLUpdate(update);
    LOG.info("SQL Update statement:" + sqlUpdateStmt);
  }

}
