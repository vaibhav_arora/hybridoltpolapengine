package databaseDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import junit.framework.Assert;

import messaging.HybridEngine.SerializedTransaction;

import org.junit.Before;
import org.junit.Test;

import utilities.Operation;
import utilities.Transaction;

public class MonetDBDriverTest {
  
  String dbUrl;
  String user;
  String password;
  MonetDBDriver monetDBDriver;

  @Before
  public void setUp(){
    dbUrl = "jdbc:monetdb://127.0.0.1/test";
    user = "monetdb";
    password = "monetdb";
  }
  
  //Uncomment the next line to test if MonetDb can be connected to 
  //@Test
  public void connect() {
    try {
      Class.forName("nl.cwi.monetdb.jdbc.MonetDriver");
      Connection conn = DriverManager.getConnection(dbUrl, user, password);
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }  catch (ClassNotFoundException c){
    	
    }
  }
  
  @Test
  public void performExecuteQueryTest() {
    monetDBDriver = new MonetDBDriver(dbUrl, user, password, false);
    String readQuery = "Select value from TestTable where table_key = 1";
    Operation queryOperation = new Operation(readQuery);
   
    monetDBDriver.executeQuery(queryOperation);
  }
  
  @Test
  public void performBatchCommitTest() {
    monetDBDriver = new MonetDBDriver(dbUrl, user, password, false);
    
    //String update1 = "Update TestTable SET value='Value1d' Where table_key = 1";
    String update1 = "Write test#TestTable#table_key#1#value#'Value1d'";
    
    //String update2 = "Update TestTable SET value='Value2d' Where table_key = 2";
    String update2 = "Write test#TestTable#table_key#2#value#'Value2d'";
    
    //String update3 = "Update TestTable SET value='Value3d' Where table_key = 3";
    String update3 = "Write test#TestTable#table_key#3#value#'Value3d'";
    
    SerializedTransaction.Builder testTransaction = SerializedTransaction.newBuilder().
        setTransactionId(-1);
    
    testTransaction.addUpdateOperation(update1);
    testTransaction.addUpdateOperation(update2);
    testTransaction.addUpdateOperation(update3);
    
    Boolean didCommit = monetDBDriver.batchCommit(testTransaction.build());
    Assert.assertTrue(didCommit);
    
  }

}
