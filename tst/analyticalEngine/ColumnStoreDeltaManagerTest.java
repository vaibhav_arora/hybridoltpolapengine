package analyticalEngine;

import java.util.ArrayList;
import java.util.List;

import messaging.HybridEngine.SerializedTransaction;

import org.junit.Before;
import org.junit.Test;

import utilities.TransactionBatch;
import databaseDriver.MonetDBDriver;

public class ColumnStoreDeltaManagerTest {
  
  ColumnStoreDeltaManager columnStoreDeltaManager;
  
  MonetDBDriver monetDBDriver;
  
  String analyticalDBUrl;
  String analyticalDBuser;
  String analyticalDBpassword;
  
  @Before
  public void setUp() {
    
    analyticalDBUrl = "jdbc:monetdb://127.0.0.1/test";
    analyticalDBuser = "monetdb";
    analyticalDBpassword = "monetdb";
    boolean monetDBAutoCommit = false;
    
    monetDBDriver = new MonetDBDriver(analyticalDBUrl, analyticalDBuser, 
        analyticalDBpassword, monetDBAutoCommit);
    
    int batchIdToIngest = 1;
    int partitionSize = 1;
    columnStoreDeltaManager = new ColumnStoreDeltaManager
        (batchIdToIngest, partitionSize, monetDBDriver);
    
  }
  
  @Test
  public void testIngestTransactionBatchToDelta() {
    int partitionID = 1;
    
    int batchID = 1;
    List<SerializedTransaction> serializedTransactionList = new ArrayList<SerializedTransaction>();
    
    SerializedTransaction.Builder testTransaction1 = SerializedTransaction.newBuilder().
        setTransactionId(-1);
    
    String update1 = "Write test#TestTable#table_key#1#value#'ColValue1d'";
    
    testTransaction1.addUpdateOperation(update1);
    serializedTransactionList.add(testTransaction1.build());
    

    SerializedTransaction.Builder testTransaction2 = SerializedTransaction.newBuilder().
        setTransactionId(-2);
    String update2 = "Write test#TestTable#table_key#2#value#'ColValue2d'";
    
    testTransaction2.addUpdateOperation(update2);
    serializedTransactionList.add(testTransaction2.build());
    
    // Transaction Ids could have been same for all transactions for testing purposes
    SerializedTransaction.Builder testTransaction3 = SerializedTransaction.newBuilder().
        setTransactionId(-3);
    String update3 = "Write test#TestTable#table_key#3#value#'ColValue3d'";
    
    testTransaction3.addUpdateOperation(update3);
    serializedTransactionList.add(testTransaction3.build());
  
    TransactionBatch transactionBatch;
    
    Boolean hasDistributedTransactions = false;
    transactionBatch = new TransactionBatch(batchID, serializedTransactionList, 
    		hasDistributedTransactions);
    
    columnStoreDeltaManager.ingestChangesToColumnStore(partitionID, transactionBatch);
  }

}
